DELETE `a`, `b`, `c`, `z`
FROM `b_sale_basket` AS `a`
    LEFT JOIN `b_sale_order_change` AS `b`
        ON `b`.`ORDER_ID` = `a`.`ORDER_ID`
    LEFT JOIN `b_sale_order` AS `c`
        ON `c`.`ID` = `a`.`ORDER_ID`
    LEFT JOIN `b_sale_basket_props` AS `z`
        ON `z`.`BASKET_ID` = `a`.`id`
WHERE a.DATE_UPDATE<='2014-01-01 00:00:00'
