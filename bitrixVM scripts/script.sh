ENCODIN='utf-8' ;
#
# Get Directory name
#
echo "Directory name for website files (e.g.: mysite): "
read sitedir ;

mkdir -p /home/bitrix/ext_www/$sitedir/public_html
mkdir -p /tmp/php_sessions/ext_www/$sitedir/public_html
mkdir -p /tmp/php_upload/ext_www/$sitedir/public_html

cp -rp /install/files_install/* /home/bitrix/ext_www/$sitedir/public_html
cp /install/admin_inform.txt /home/bitrix/ext_www/$sitedir
cd /home/bitrix/ext_www/$sitedir/public_html

ftpUser=$sitedir"_ftp"
ftpPass=`< /dev/urandom /usr/bin/tr -dc _A-Z-a-z-0-9 | /usr/bin/head -c10`

useradd -d /home/bitrix/ext_www/$sitedir -s /sbin/nologin "$ftpUser"

usermod -G bitrix "$ftpUser"
#groups "$ftpUser"

chmod 770 /home/bitrix/ext_www/"$sitedir"/

chown -R "$ftpUser":bitrix /home/bitrix/ext_www/"$sitedir"/
find /home/bitrix/ext_www/"$sitedir" -type f -exec chmod 664 {} +
find /home/bitrix/ext_www/"$sitedir" -type d -exec chmod 775 {} +
echo "$ftpPass" | passwd --stdin "$ftpUser"

# tar -zxf SiteSetup.tar.gz
# rm -f SiteSetup.tar.gz
phpSessionSavePath="\/tmp\/php_sessions\/ext_www\/$sitedir/public_html"
phpUploadTmpDir="\/tmp\/php_upload\/ext_www\/$sitedir/public_html"

chown -R bitrix:bitrix /home/bitrix/ext_www/$sitedir
chmod -R 0770 /home/bitrix/ext_www/$sitedir

chown -R bitrix:bitrix /tmp/php_sessions/ext_www/$sitedir
chmod -R 0770 /tmp/php_sessions/ext_www/$sitedir

chown -R bitrix:bitrix /tmp/php_upload/ext_www/$sitedir
chmod -R 0770 /tmp/php_upload/ext_www/$sitedir
#
# Get Site name
#
sitename=$sitedir".dev-bitrix.by" ;
sitename_alias=$sitedir"-dev.greenkevich.by" ;

newName="$sitename www.$sitename;" ;
encoding="$ENCODIN";
character="DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci";
charset="$ENCODIN";
phpOverload=" ";
phpInternal=" ";
#
# Get DBLogin, DBPasswd, DBName
#
dbHost='localhost'
DBLogin="$sitedir"
DBPasswd=','`< /dev/urandom /usr/bin/tr -dc _A-Z-a-z-0-9 | /usr/bin/head -c10`

DBName=$sitedir"_db"
res=`mysql -u root -pgreenkevich.by -e "CREATE DATABASE ${DBName} ${character};"` ;
res=`mysql -u root -pgreenkevich.by -e "GRANT ALL PRIVILEGES ON ${DBName}.* TO '${DBLogin}'@'%' IDENTIFIED BY '$DBPasswd'; GRANT ALL PRIVILEGES ON root.* TO '${DBLogin}'@'localhost' IDENTIFIED BY '$DBPasswd';"` ;

sed -i "s/#DB#/$DBName/g" /home/bitrix/ext_www/$sitedir/public_html/bitrix/php_interface/dbconn.php
sed -i "s/#loginDB#/$DBLogin/g" /home/bitrix/ext_www/$sitedir/public_html/bitrix/php_interface/dbconn.php
sed -i "s/#passDB#/$DBPasswd/g" /home/bitrix/ext_www/$sitedir/public_html/bitrix/php_interface/dbconn.php
sed -i "s/#DBHost#/$dbHost/g" /home/bitrix/ext_www/$sitedir/public_html/bitrix/php_interface/dbconn.php
phpSessionSavePath="\/tmp\/php_sessions\/ext_www\/$sitedir"
phpUploadTmpDir="\/tmp\/php_upload\/ext_www\/$sitedir"

#
# Setting HTTPD,NGINX,HOSTS
#
read num </install/num.txt ;
location='127.0.0.'$num ;
localip=$location':8887' ;

num=$((num+1)) ;
echo $num >/install/num.txt ;

cp /install/bx_nginx_site_template.conf /etc/nginx/bx/site_avaliable/bx_ext_"$sitedir".conf
sed -i "s/#LOCAlIP#/$localip/g" /etc/nginx/bx/site_avaliable/bx_ext_"$sitedir".conf
sed -i "s/#SERVER_NAME#/$sitename/g" /etc/nginx/bx/site_avaliable/bx_ext_"$sitedir".conf
sed -i "s/#SERVER_NAME_ALIAS#/$sitename_alias/g" /etc/nginx/bx/site_avaliable/bx_ext_"$sitedir".conf
sed -i "s/#SERVER_DIR#/$sitedir/g" /etc/nginx/bx/site_avaliable/bx_ext_"$sitedir".conf
sed -i "s/#SERVER_ENCODING#/$charset/g" /etc/nginx/bx/site_avaliable/bx_ext_"$sitedir".conf
ln -sf /etc/nginx/bx/site_avaliable/bx_ext_"$sitedir".conf /etc/nginx/bx/site_ext_enabled

cp /install/bx_ssl_nginx_site_template.conf /etc/nginx/bx/site_avaliable/bx_ext_ssl_"$sitedir".conf
sed -i "s/#LOCAlIP#/$localip/g" /etc/nginx/bx/site_avaliable/bx_ext_ssl_"$sitedir".conf
sed -i "s/#SERVER_NAME#/$sitename/g" /etc/nginx/bx/site_avaliable/bx_ext_ssl_"$sitedir".conf
sed -i "s/#SERVER_NAME_ALIAS#/$sitename_alias/g" /etc/nginx/bx/site_avaliable/bx_ext_ssl_"$sitedir".conf
sed -i "s/#SERVER_DIR#/$sitedir/g" /etc/nginx/bx/site_avaliable/bx_ext_ssl_"$sitedir".conf
sed -i "s/#SERVER_ENCODING#/$charset/g" /etc/nginx/bx/site_avaliable/bx_ext_ssl_"$sitedir".conf
ln -sf /etc/nginx/bx/site_avaliable/bx_ext_ssl_"$sitedir".conf /etc/nginx/bx/site_ext_enabled

cp /install/bx_apache_site_template.conf     /etc/httpd/bx/conf/bx_ext_"$sitedir".conf
sed -i "s/#LOCAlIP#/$localip/g"             /etc/httpd/bx/conf/bx_ext_"$sitedir".conf
sed -i "s/#SERVER_NAME#/$sitename/g" /etc/httpd/bx/conf/bx_ext_"$sitedir".conf
sed -i "s/#SERVER_NAME_ALIAS#/$sitename_alias/g" /etc/httpd/bx/conf/bx_ext_"$sitedir".conf
sed -i "s/#SERVER_DIR#/$sitedir/g" /etc/httpd/bx/conf/bx_ext_"$sitedir".conf
sed -i "s/#PHP_OVERLOAD#/$phpOverload/g" /etc/httpd/bx/conf/bx_ext_"$sitedir".conf
sed -i "s/#PHP_INTERNAL#/$phpInternal/g" /etc/httpd/bx/conf/bx_ext_"$sitedir".conf
sed -i "s/#PHP_SESSIONS#/php_admin_value session.save_path $phpSessionSavePath/g" /etc/httpd/bx/conf/bx_ext_"$sitedir".conf
sed -i "s/#PHP_UPLOAD#/php_admin_value upload_tmp_dir $phpUploadTmpDir/g" /etc/httpd/bx/conf/bx_ext_"$sitedir".conf

echo "$location $sitename" >> /etc/hosts ;

#
# Sending Data
#

echo "Site Administrator email:"
read email ;

sed -i "s/#SITE_ADMIN#/admin/g" /home/bitrix/ext_www/$sitedir/admin_inform.txt
sed -i "s/#SITE_ADMIN_PASSWORD#/$DBPasswd/g" /home/bitrix/ext_www/$sitedir/admin_inform.txt
sed -i "s/#SITE_ADMIN_EMAIL#/$email/g" /home/bitrix/ext_www/$sitedir/admin_inform.txt
sed -i "s/#SITE#/$sitename/g" /home/bitrix/ext_www/$sitedir/admin_inform.txt
sed -i "s/#DB#/$DBName/g" /home/bitrix/ext_www/$sitedir/admin_inform.txt
sed -i "s/#loginDB#/$DBLogin/g" /home/bitrix/ext_www/$sitedir/admin_inform.txt
sed -i "s/#passDB#/$DBPasswd/g" /home/bitrix/ext_www/$sitedir/admin_inform.txt
sed -i "s/#DBHost#/$dbHost/g" /home/bitrix/ext_www/$sitedir/admin_inform.txt
sed -i "s/#FTP_LOGIN#/$ftpUser/g" /home/bitrix/ext_www/$sitedir/admin_inform.txt
sed -i "s/#FTP_PASS#/$ftpPass/g" /home/bitrix/ext_www/$sitedir/admin_inform.txt

php -r "mail('$email', 'SITE INFORMER', '
	* http://$sitename/bitrix
	* admin
	* $DBPasswd
	* $dbHost
	* $DBLogin
	* $DBPasswd
	* $DBName
	* $ftpUser
	* $ftpPass
	 ');"
#
# RESTART
#
service httpd restart
service nginx restart
