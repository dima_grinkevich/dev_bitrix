site=$1
echo "----------${site}-----------";

if [ ! -d /home/bitrix/ext_www/${site}/bitrix/ -a ! -d /home/bitrix/ext_www/${site}/public_html/bitrix/ ]; then
    echo "the site ${site} is not on the Bitrix";
    exit;
fi

# echo "site ${site}";
# exit;
if [ -d /home/bitrix/ext_www/${site} ]; then
    echo "start backup site ${site}";

    data=`/bin/date +%d.%m.%Y`

    backup_folder=/home/bitrix/backup/archive/${data};

    test ! -d $backup_folder && { mkdir -p $backup_folder ; }

    dbsettings=`php -f /home/bitrix/backup/scripts/get_mysql_settings.php site=$site`;
    #echo $dbsettings;
    iCnt=0;
    for settings in $dbsettings
    do

    	arSettings[$iCnt]=$settings;
    	let "iCnt++";
    done

    i=0;
    if [ "${arSettings[$i]}" != "mysql" ]; then
    	exit;
    fi

    i=1;
    mysqlHost=""
    if [ "${arSettings[$i]}" != "localhost" ]; then
    	mysqlHost="-h ${arSettings[$i]} ";
    fi

    i=2;
    mysqlUser="${arSettings[$i]}";
    if [ "$mysqlUser" = "" ]; then
    	exit;
    fi

    i=3;
    mysqlDB="${arSettings[$i]}";
    if [ "$mysqlDB" = "" ]; then
    	exit;
    fi

    i=4;
    characterSet="${arSettings[$i]}";
    if [ "$characterSet" = "" ]; then
    	exit;
    fi

    i=5;
    mysqlPassword=""
    if [ "${arSettings[$i]}" != "" ]; then
    	mysqlPassword="${arSettings[$i]}";
    fi

    function randString ()
    {
    	local randLength
    	if [ $1 ]; then
    		randLength=$1
    	else
    		randLength=8
    	fi
    	rndStr=</dev/urandom tr -dc A-Za-z0-9 | head -c $randLength
    	echo $rndStr
    }
    echo 'start dump sql';

    randFName=`randString`;
    sql_path=/home/bitrix/backup/mysql_dump_${site}_${data}_${randFName}.sql;
    sql_path_acon=/home/bitrix/backup/mysql_dump_${site}_${data}_${randFName}_after_connect.sql;

    if [ "$mysqlPassword" = "" ]; then
        mysqldump --user=${mysqlUser} ${mysqlHost} --default-character-set=${characterSet} ${mysqlDB} > $sql_path
    else
        mysqldump --user=${mysqlUser} ${mysqlHost} -p${mysqlPassword} --default-character-set=${characterSet} ${mysqlDB} > $sql_path
    fi

    if [ "$_dump_char" == "cp1251" ]; then
    	echo "SET NAMES 'cp1251' COLLATE 'cp1251_general_ci';" > $sql_path_acon
    else
    	echo "SET NAMES 'utf8' COLLATE 'utf8_unicode_ci';" > $sql_path_acon
    fi
    #tar -czvf ${sql_path}.tar.gz ${sql_path} 2> /dev/null

    echo 'start end dump sql';

    echo 'start arhive site';
    cd /home

    tar -cf ${backup_folder}/www_backup_${site}_${data}_${randFName}.tar --exclude-from=/home/bitrix/backup/scripts/ex.txt -C /home/bitrix/ext_www/${site} \
    	. \
    	2>/dev/null

    tar -rf ${backup_folder}/www_backup_${site}_${data}_${randFName}.tar bitrix/backup/mysql_dump_${site}_${data}_${randFName}.sql bitrix/backup/mysql_dump_${site}_${data}_${randFName}_after_connect.sql;  2>/dev/null

    gzip ${backup_folder}/www_backup_${site}_${data}_${randFName}.tar

    rm -f $sql_path
    rm -f $sql_path_acon
    echo "end backup site ${site}";

    #
    # mkdir ${backup_folder}/www_backup_${site}_${data}_${randFName}
    #
    # tar vxzf ${backup_folder}/www_backup_${site}_${data}_${randFName}.tar.gz -C ${backup_folder}/www_backup_${site}_${data}_${randFName}/
else
    echo 'not site';
fi
