#!/bin/sh

PROGPATH=$(dirname $0)


folders=`ls -G /home/bitrix/ext_www`;
cntSiteAll=`ls -G /home/bitrix/ext_www | wc -l`;
iCntSite=1;
for site in $folders
do
	echo "site start ${iCntSite} / ${cntSiteAll}"
	/home/bitrix/backup/scripts/backupsite.sh $site;
	let "iCntSite++";
done
