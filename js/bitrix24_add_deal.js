<script>
var popupTest;
	BX.addCustomEvent('onPopupShow', function(popup) {
		if(popup.uniquePopupId == 'menu-popup-task-view-b'){
			popupTest = popup;
			var dealButton = BX.findChild(
				popup.contentContainer,
				{className: 'crm_deal'},
				true
			);

			var isDealButton = dealButton === null;
			var isDealInTask = document.querySelector('[id^="balloon_DEAL_"]') === null;
			if(isDealButton && isDealInTask){


				var arHref = window.location.pathname.split('/');
				var taskId = parseInt(arHref[arHref.length-2]);

				var item = {
				    className: 'crm_deal',
				    title: 'Создать сделку',
				    text: 'Создать сделку',
				    onclick: 'setDealForTask(' + taskId +')',
				};

				var block = BX.create(!!item.href ? "a" : "span", {
				    props : { className: "menu-popup-item" +  (BX.type.isNotEmptyString(item.className) ? " " + item.className : " menu-popup-no-icon")},
				    attrs : { title : item.title ? item.title : "", onclick: item.onclick && BX.type.isString(item.onclick) ? item.onclick : "", target : item.target ? item.target : "" },
				    events : item.onclick && BX.type.isFunction(item.onclick) ? { click : BX.proxy(this.onItemClick, {obj : this, item : item }) } : null,
				    children : [
				        BX.create("span", { props : { className: "menu-popup-item-left"} }),
				        BX.create("span", { props : { className: "menu-popup-item-icon"} }),
				        BX.create("span", { props : { className: "menu-popup-item-text"}, html : item.text }),
				        BX.create("span", { props : { className: "menu-popup-item-right"} })
				    ]
				});


				BX.findChild(
				    popup.contentContainer,
				    {className: 'menu-popup-items'},
				    true
				).appendChild(block);
			}
		}

	});

	function setDealForTask(taskId)
	{
		if(taskId > 0){
			BX.ajax.post(
				'/ajax/ajax_action_task.php',
				{
					sessid: BX.message('bitrix_sessid'),
					TASK_ID : taskId
				},
				function(data)
				{
					console.log(data);
					if(data != 'error'){
						window.location = "/crm/deal/show/"+data+"/";
					}else{
						alert(data);
					}
				}
			);
		}
	}
</script>
