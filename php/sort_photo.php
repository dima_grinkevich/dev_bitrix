<?
define('_DS', DIRECTORY_SEPARATOR);
$path = __DIR__._DS."img"._DS;

$handle = @opendir($path);
if ($handle)
{
	$arFiles = array();
	while ($file = readdir($handle))
	{
		if (in_array($file, array(".", "..")) || is_dir($path.$file))
			continue;
		//echo '<pre>'.print_r($file ,true).'</pre>';
		$rs = exif_read_data($path.$file);
		$arFiles[$rs['FileDateTime']] = array('PATH' => $path.$rs['FileName'], 'NAME' => explode('.', $rs['FileName']));
	}
}
ksort($arFiles);


$i = 0;
foreach($arFiles as $file){
	rename($file['PATH'], $path.'tmp'._DS.$i++.'.'.$file['NAME'][1]);
}