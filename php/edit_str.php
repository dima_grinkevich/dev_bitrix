<?
function edit_str($str, $br = false, $tag = 'span', $class = 'big'){
	$arStr = explode(' ', $str);
	if(strlen($class) > 0){
		$class = ' class="'.$class.'"';
	}
	$arStr[0] = '<'.$tag.$class.'>'.$arStr[0].'</'.$tag.'>';
	$arStr[0] .= $br ? '</br>' : '';
	$str = implode(' ', $arStr);
	
	return $str;
}
?>