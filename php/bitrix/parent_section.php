<?

function getParentTreeList($iSectID, $iIBlockID, $arrFilter = array(), $arSelect = array()){
	CModule::IncludeModule('iblock');
	$SECTION_ID = intval($iSectID);
	$IBLOCK_ID = intval($iIBlockID);

	$arSection = array();
	$bWrite = false;
	$iPreviousLevel = 0;
	$arFilter = array('IBLOCK_ID' => $IBLOCK_ID);
	$rs = CIBlockSection::GetList(array('left_margin' => 'desc'), array_merge($arFilter, $arrFilter), false, $arSelect);
	while($ar = $rs->Fetch()){
		if($ar['ID'] == $SECTION_ID){
			$bWrite = true;
		}
		if($bWrite){
			if(!$iPreviousLevel || $iPreviousLevel > $ar['DEPTH_LEVEL']){
				if(count($arSection) <= 0){
					$arSection = $ar;
				}else{
					$arSection = array_merge($ar, array('CHIELD' => $arSection ));
				}
				$iPreviousLevel = $ar['DEPTH_LEVEL'];
			}
		}
		if($bWrite && $ar['DEPTH_LEVEL'] == 1){
			break;
		}
	}
	return $arSection;
}