<?
AddEventHandler("tasks", "OnBeforeTaskAdd", array("CGDSTaskEvent", "OnBeforeTaskAdd"));

Class CGDSTaskEvent{
	private static $arRule = array(
		array(
			'work' => 'work1',
			'crm' => array(
				'C_21', //Контакт: Contact_21
				'CO_11' //Компания: Company_11
			),
			'group' => 11 //Группа (проект)
		),
		array(
			'work' => 'test1',
			'crm' => array(
				'C_19', //Контакт: Contact_19
				'CO_11' //Компания: Company_11
			),
			'group' => 11 //Группа (проект)
		)
	);

	function OnBeforeTaskAdd(&$arFields){

        /* проставлять по слову в названии задачи Контакты+Компания из CRM и группу*/
		$arTitle = explode(' ', ToLower($arFields['TITLE']));

		foreach(self::$arRule as $rule){
			$arWork = explode(';', ToLower($rule['work']));
			$result = array_intersect ($arWork, $arTitle);
			if(count($result) > 0){
				if(intval($arFields['GROUP_ID']) < 1 && intval($rule['group']) > 0)
					$arFields['GROUP_ID'] = $rule['group'];

				$arFields['UF_CRM_TASK'] = array_merge((array)$arFields['UF_CRM_TASK'], (array)$rule['crm']);
				break;
			}
		}

		$arFields['TASK_CONTROL'] = 'Y'; //принять после завершения

        //проставлять группу для под задач, если не задана
		$parentId = intval($arFields['PARENT_ID']);
		$groupId = intval($arFields['GROUP_ID']);
		if($parentId > 0 && $groupId < 1){
			$rsParentTask = CTasks::GetList(
				false,
				array('ID' => $arFields['PARENT_ID']),
				array('ID', 'GROUP_ID', 'UF_CRM_TASK')
			);
			if($arParentTask = $rsParentTask->Fetch()){
				$arFields['GROUP_ID'] = $arParentTask['GROUP_ID'];
				if(!empty($arParentTask['UF_CRM_TASK'])){
					foreach($arParentTask['UF_CRM_TASK'] as $v){
						if(strpos($v, 'C_') === 0 || strpos($v, 'CO_') === 0){
							$arFields['UF_CRM_TASK'][] = $v;
						}
					}
				}
			}
		}
	}
}
