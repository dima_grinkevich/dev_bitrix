<?

//put down the country and the city, depending on the domain mailbox

CModule::IncludeModule('crm');
$obContact = new CCrmContact();

$rs = $obContact->GetListEx( array(),
   array('CHECK_PERMISSIONS' => 'N', 'ADDRESS_CITY' => false),
    false,
    false
);
while($ar = $rs->Fetch()){
   $arUpdate = array();
   $multifields = \Bitrix\Crm\Integrity\DuplicateCommunicationCriterion::prepareEntityMultifieldsValues(
      CCrmOwnerType::Contact,
      $ar['ID']
   );
   $emails = \Bitrix\Crm\Integrity\DuplicateCommunicationCriterion::extractMultifieldsValues($multifields, 'EMAIL');
   if(!empty($emails)){
      foreach($emails as $email){
         if(strpos($email, '.by') !== false){
            $arUpdate = array('ADDRESS_CITY' => 'Минск', 'ADDRESS_COUNTRY' => 'Беларусь');
            break;
         }
         if(strpos($email, '.ru') !== false){
            $arUpdate = array('ADDRESS_CITY' => 'Москва', 'ADDRESS_COUNTRY' => 'Россия');
         }
      }
      if(!empty($arUpdate)){
         $obContact->Update($ar['ID'], $arUpdate);
      }
   }
}
