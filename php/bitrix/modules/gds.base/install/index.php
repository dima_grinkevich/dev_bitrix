<?
IncludeModuleLangFile(__FILE__);

Class gds_base extends CModule
{
	const MODULE_ID = 'gds.base';
	var $MODULE_ID = 'gds.base'; 
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';
	var $NEED_MAIN_VERSION = '12.0';

	function __construct()
	{
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("gds.base_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("gds.base_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("gds.base_PARTNER_NAME");
		//$this->PARTNER_URI = GetMessage("gds.base_PARTNER_URI");
	}

	function InstallDB($arParams = array())
	{
	
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		return true;
	}

	function UnInstallFiles()
	{
		return true;
	}

	function DoInstall()
	{
		$this->InstallFiles();
		$this->InstallEvents();
		$this->InstallDB();
		RegisterModule(self::MODULE_ID);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		UnRegisterModule(self::MODULE_ID);
		$this->UnInstallEvents();
		$this->UnInstallDB();
		$this->UnInstallFiles();
	}
}
?>