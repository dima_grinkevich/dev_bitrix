<?

CModule::AddAutoloadClasses(
	"gds.base",
	array(
		"GDSBase" =>  "classes/general/base.php",
	)
);

function resizeImage($id, $width, $height, $type = BX_RESIZE_IMAGE_PROPORTIONAL_ALT){
	return array_change_key_case(CFile::ResizeImageGet(
			$id, 
			array("width"=>$width, "height"=>$height), 
			$type, 
			true, 
			false, 
			false, 
			100
		), CASE_UPPER);
}