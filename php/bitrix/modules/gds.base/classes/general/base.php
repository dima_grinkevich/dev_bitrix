<? 
IncludeModuleLangFile(__FILE__); 

/** 
 * Class with basic functions
 */ 
class GDSBase 
{ 
    public static function GDSBaseShowClassContent($prop = 'ClassContent') 
    { 
        global $APPLICATION; 
        $flgShow = $APPLICATION->GetProperty($prop, 'N') != 'N' ? true : false; 
        if($flgShow){ 
            if ($APPLICATION->GetPageProperty($prop)) 
                return $APPLICATION->GetPageProperty($prop); 
         
        }else{
            return 'contNew';
		} 
    } 

      
    /** 
     * forms date back day, 1 hour ago, a month ago
     * @param string $strDate 
     * @param boolean $now 
     * @param array $arFormat 
     * @param const string $DATE_FORMAT 
     * @return string 
     */ 
    public static function CreateDate($strDate, $now=false, $arFormat = array(), $DATE_FORMAT = FORMAT_DATETIME) 
    { 
        if($now === false) $now = time(); 
        return ToLower(FormatDate( 
                ( 
                        count($arFormat) > 0 
                        ? $arFormat 
                        : 
                        array( 
                                "tommorow" => "H:m, tommorow", 
                                "s" => "sago", 
                                "i" => "iago", 
                                "H" => "Hago", 
                                "today" => "H:m, today", 
                                "yesterday" => "H:m, yesterday", 
                                "dago" => "dago", 
                                "d" => 'j F', //'H:m, j F', 
                                "m" => 'j F', 
                                "" => 'j F Y', 
                        ) 
                ), 
                MakeTimeStamp($strDate, $DATE_FORMAT),  $now 
        )); 
    } 

    /** 
     * Form the size of the file Mb, Gb....
     * @param int $intSize 
     * @param number $round 
     * @return string 
     */ 
    public static function GetStrFileSize($intSize, $round=0) 
    { 
        $arSize = array( 
                GetMessage("GDS_MODUL_FILE_SIZE_B"), 
                GetMessage("GDS_MODUL_FILE_SIZE_KB"), 
                GetMessage("GDS_MODUL_FILE_SIZE_MB"), 
                GetMessage("GDS_MODUL_FILE_SIZE_GB"), 
                GetMessage("GDS_MODUL_FILE_SIZE_TB"), 
                GetMessage("GDS_MODUL_FILE_SIZE_PB"), 
                GetMessage("GDS_MODUL_FILE_SIZE_EB"), 
                GetMessage("GDS_MODUL_FILE_SIZE_ZB"), 
                GetMessage("GDS_MODUL_FILE_SIZE_YB") 
        ); 
        for ($i=0; $intSize > 1024 && $i < count($arSize) - 1; $i++) $intSize /= 1024; 

        return round($intSize, $round)." ".$arSize[$i]; 

        for ($i=0; $size > 1024 && $i < count($sizes) - 1; $i++) $size /= 1024; 
        return round($size,$round)." ".$sizes[$i]; 
    } 

    /** 
     * Form the class for the file type icons
     * @param string $type 
     * @param string $def 
     * @return string 
     */ 
    public static function GetStrFileType($type, $def='') 
    { 
        switch($type){ 
            case 'docx': case 'doc': case 'rtf': return 'doc'; break; 
            case 'xlsx': case 'xls': return 'xls'; break; 
            case 'pptx': case 'ppt': return 'ppt';     break; 
            case 'pdf': return 'pdf'; break; 
            case 'txt': return 'txt'; break; 
            //case 'rtf': return 'rtf'; break; 
            case 'odt': return 'odt'; break; 
            case 'ods': return 'ods'; break; 
            case 'odp': return 'odp'; break; 
            case 'zip': return 'zip'; break; 
            case 'rar': return 'rar'; break; 
            case 'jpeg': case 'jpg': return 'jpg'; break; 
            case 'gif': return 'gif'; break; 
            case 'png': return 'png'; break; 
            case 'png': return 'png'; break; 
            default: 
                return $def; 
                break; 
        } 
    } 
} 
?>