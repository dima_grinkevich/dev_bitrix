<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule('iblock');
/* Start Params checked */
if(strlen($arParams['BASKET_ID']) <= 0)
	$arParams['BASKET_ID'] = 'basket';

$arParams['PRICE_CODE_PARAM'] = 'BASE';	

$arParams["PRICE_CODE"] = array($arParams['PRICE_CODE_PARAM']);

$arParams['PRICE_VAT_INCLUDE'] = 'Y';
$arParams['IBLOCK_NUMBERS'] = intval($arParams['IBLOCK_NUMBERS']);
if($arParams['IBLOCK_NUMBERS'] <= 0)
	$arParams['IBLOCK_NUMBERS'] = 1;

$arParams['CONVERT_CURRENCY'] = 'Y'; //$arParams['CONVERT_CURRENCY'] == 'Y' ? 'Y' : 'N';
$arParams['CURRENCY_ID'] = 'RUB';

/* End Params checked */

$arResult['BASKET'] = array();

$sBasketID = $arParams['BASKET_ID'];
$arTmp = (array)json_decode($_COOKIE[$sBasketID]);
$cntBasket = count($arTmp);
if($cntBasket > 0){
	$arConvertParams = array();
	if ('Y' == $arParams['CONVERT_CURRENCY'])
	{
		if (!CModule::IncludeModule('currency'))
		{
			$arParams['CONVERT_CURRENCY'] = 'N';
			$arParams['CURRENCY_ID'] = '';
		}
		else
		{
			$arCurrencyInfo = CCurrency::GetByID($arParams['CURRENCY_ID']);
			if (!(is_array($arCurrencyInfo) && !empty($arCurrencyInfo)))
			{
				$arParams['CONVERT_CURRENCY'] = 'N';
				$arParams['CURRENCY_ID'] = '';
			}
			else
			{
				$arParams['CURRENCY_ID'] = $arCurrencyInfo['CURRENCY'];
				$arConvertParams['CURRENCY_ID'] = $arCurrencyInfo['CURRENCY'];
			}
		}
	}
	
	$arIblocks = array();
	$arIDs = array();

	//$arResult["PRICES"] = CIBlockPriceTools::GetCatalogPrices($arParams["IBLOCK_ID"], $arParams["PRICE_CODE"]);
	foreach($arTmp as $k => $arVal){
		$arIDs[] = $arVal->id;
	}
	
	
	
	$arFilter = array('ID' => $arIDs);
	$arSelect = array(
		"ID",
		"IBLOCK_ID",
		"CODE",
		"XML_ID",
		"NAME",
		"ACTIVE",
		"DATE_ACTIVE_FROM",
		"DATE_ACTIVE_TO",
		"SORT",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"DETAIL_TEXT",
		"DETAIL_TEXT_TYPE",
		"DATE_CREATE",
		"CREATED_BY",
		"TIMESTAMP_X",
		"MODIFIED_BY",
		"TAGS",
		"IBLOCK_SECTION_ID",
		"DETAIL_PAGE_URL",
		"DETAIL_PICTURE",
		"PREVIEW_PICTURE",
		"PROPERTY_*",
	);
	
	$iblocks = GetIBlockList("catalog");
	while($arIBlock = $iblocks->GetNext()) //���� �� ���� ������
	{
		$arIblocks[] = $arIBlock['ID'];
	}
	foreach($arIblocks as $id){
		$arPrices = CIBlockPriceTools::GetCatalogPrices($id, $arParams["PRICE_CODE"]);
		foreach($arPrices as $type){
			$arSelect[$type["SELECT"]] = $type["SELECT"];
		}
		$arResult["PRICES"][$id] = $arPrices;
	}
	
	$arItems = array();
	$arSectionsID = array();
	
	$rs = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	while($ar = $rs->GetNextElement()){
		$arElement = $ar->GetFields();
		$arElement['PROPERTIES'] = $ar->GetProperties();
		$arElement["PRICES"] = CIBlockPriceTools::GetItemPrices($arElement["IBLOCK_ID"], $arResult["PRICES"][$arElement["IBLOCK_ID"]], $arElement, $arParams['PRICE_VAT_INCLUDE'], $arConvertParams);
		
		if($arElement['IBLOCK_ID'] == $arParams['IBLOCK_NUMBERS'] && $arElement['IBLOCK_SECTION_ID'] > 0){
			$arSectionsID[] = $arElement['IBLOCK_SECTION_ID'];
		}
		$arItems[$arElement['ID']] = $arElement;
	}	
	
	$arResult['SECTIONS'] = array();
	if(count($arSectionsID) > 0){
		$rs = CIBlockSection::GetList(array(), array('ID' => $arSectionsID));
		while($arSection = $rs->Fetch()){
			if(!in_array($arSection['ID'], $arSectionsID)) continue;
			$arResult['SECTIONS'][$arSection['ID']] = $arSection;
		}
	}

	$arResult['SUM_TOTAL'] = 0;
	foreach($arTmp as $arVal){
		$arItems[$arVal->id]['CNT_BUY'] = $arVal->cnt;
		$arResult['SUM_TOTAL'] += $arItems[$arVal->id]['PRICES'][$arParams['PRICE_CODE_PARAM']]['VALUE'];
	}
	$arResult['SUM_TOTAL_CURRENCY'] = CurrencyFormat($arResult['SUM_TOTAL'], $arParams['CURRENCY_ID']);
	
	$arResult['BASKET'] = $arItems;
}

$this->IncludeComponentTemplate();