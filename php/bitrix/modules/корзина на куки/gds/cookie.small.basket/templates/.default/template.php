<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(count($arResult['BASKET']) <= 0) return;?>

<div class="flying<?= count($arResult['BASKET']) <= 0 ? ' _hide' : '';?>">
    <table>
        <tr>
            <td>
                <div class=flying__appendiks><img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-cart"> <span
                        class=flying__price><?=$arResult['SUM_TOTAL_CURRENCY']?></span></div>
            </td>
            <td>
                <div class=flying__body>
                    <div class=flying__header><?=GetMessage('TITLE_BASKET')?>:</div>
                    <div class=flying__list>
                        <table>
							<?foreach($arResult['BASKET'] as $arElement):
								$mSection = isset($arResult['SECTIONS'][$arElement['IBLOCK_SECTION_ID']]) ? $arResult['SECTIONS'][$arElement['IBLOCK_SECTION_ID']] : false;
							?>
                            <tr>
                                <td class="td_1"><?= is_array($mSection) ? '+7 ('.$mSection["NAME"].') ' : ''?><?=$arElement['NAME']?></td>
                                <td class="td_2"><?=$arElement['PRICES'][$arParams['PRICE_CODE_PARAM']]['PRINT_VALUE']?></td>
                                <td class="td_3"><a href="#" class="delete-btn-click" title="<?=GetMessage('DELETE_ITEM')?>" data-disable-title data-id="<?=$arElement['ID']?>"> <img
                                            src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-delete2"> </a></td>
                            </tr>
							<?endforeach;?>
                        </table>
                    </div>
                     <div class=flying__block><a href="/numbers" class="link cart__donotforget cart__donotforget_number">
                            <img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-info2"> <span class=link__text> <span><?=GetMessage('PHONE_MESSAGE_1')?><br><?=GetMessage('PHONE_MESSAGE_2')?></span> </span>
                        </a></div>
                    <div class=flying__block><a href="/cart" class="button button_light"><?=GetMessage('SUBMIT_FORM')?></a></div>
                </div>
            </td>
        </tr>
    </table>
</div>