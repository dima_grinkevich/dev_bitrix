<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
    <div class="cart">
	<?foreach($arResult['BASKET'] as $arElement):
		$mSection = isset($arResult['SECTIONS'][$arElement['IBLOCK_SECTION_ID']]) ? $arResult['SECTIONS'][$arElement['IBLOCK_SECTION_ID']] : false;
	?>
      <div class="cart__item">
		<?if($mSection):?>
		<table class="cart__number">
          <tr>
            <td class="cart__name cart__name_code">+7 (<?=$mSection['NAME']?>)</td>
            <td class="cart__name cart__name_num"><?=$arElement['NAME']?></td>
            <td>
              <div class="cart__sim" data-id="<?=$arElement['ID']?>">
                <input type="radio" name="radio" id="radio1">
                <label for="radio1" class="ico radio ico-sim-regular" data-title="<?=GetMessage('BASE_SIM')?>" title="<?=GetMessage('BASE_SIM')?>"></label>

                <input type="radio" name="radio" id="radio2">
                <label for="radio2" class="ico radio ico-sim-micro" data-title="<?=GetMessage('MICRO_SIM')?>" title="<?=GetMessage('MICRO_SIM')?>"></label>

                <input type="radio" name="radio" id="radio3">
                <label for="radio3" class="ico radio ico-sim-nano"  data-title="<?=GetMessage('NANO_SIM')?>" title="<?=GetMessage('NANO_SIM')?>"></label>
              </div>
            </td>
            <td class="cart__price">
				<?=$arElement['PRICES'][$arParams['PRICE_CODE_PARAM']]['PRINT_VALUE']?>
            </td>
            <td class="cart__actions">
              <a href="#" class="cart__delete delete-btn-click" data-id="<?=$arElement['ID']?>" title="<?=GetMessage('DELETE_BTN')?>">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-delete">
              </a>
            </td>
          </tr>
        </table>
		<?else:?>
        <table class="cart__plan">
          <tr>
            <td class="cart__name"><?=$arElement['NAME']?></td>
            <td class="cart__price">
              <?=$arElement['PRICES'][$arParams['PRICE_CODE_PARAM']]['PRINT_VALUE']?>
            </td>
            <td class="cart__actions">
              <a href="#" class="cart__delete delete-btn-click" data-id="<?=$arElement['ID']?>" title="<?=GetMessage('DELETE_BTN')?>">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-delete">
              </a>
            </td>
          </tr>
        </table>
		<?endif;?>
      </div><!-- /cart__item -->
	<?endforeach;?>

      <div class="cart__item cart__item_empty">
        <table class="cart__number">
          <tr>
            <td>
              <a href="#" class="link cart__donotforget cart__donotforget_number" onclick="return false;">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-info">
                <span class="link__text">
                  <span><?=GetMessage('NOTIF_PHONE')?></span>
                </span>
              </a>
            </td>
          </tr>
        </table>
      </div><!-- /cart__item -->


      <div class="clear"></div>
    </div><!-- /cart -->
	
	 <table class="cart__next">
      <tr>
        <td class="cart__sum">
          <?=GetMessage('TOTAL_SUM')?>: <span class="cart__total"><?=$arResult['SUM_TOTAL_CURRENCY']?></span>
        </td>
        <td class="right">
          <a href="<?=$APPLICATION->GetCurPageParam('action=delivery', array('action'));?>" class="button button_large"><?=GetMessage('DELIVERY_TYPE')?></a>
        </td>
      </tr>
    </table>