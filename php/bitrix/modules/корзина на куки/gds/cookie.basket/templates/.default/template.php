<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="module container">
	<?if($arResult['NOT_ITEMS'] == 'Y'):?>
		<p><?=GetMessage('NOT_ITEMS')?></p>
	<?else:?>
		<?require_once('header.php');?>
		<div class="module__content">
			<?if($arResult['STEP'] == 0):?>
				<?require_once('summary.php');?>
			<?elseif($arResult['STEP'] == 1):?>
				<?require_once('delivery.php');?>
			<?elseif($arResult['STEP'] == 2):?>
				<?require_once('success.php');?>
			<?endif;?>
		</div><!-- /module__content -->
	<?endif;?>
</div>