<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<div class="cart">
      <form class="cart__form form" id="delivery" method="POST">
        <table class="fixed form__block form__block_small">
          <tr>
            <td>
              <div class="form__input_with_ico" >
                <input type="text" class="form__input" required="Y" name="NAME" placeholder="<?=GetMessage('DELIVERY_NAME')?>">
                <img src="/images/px-2daeaa8b.gif" class="ico ico-person">
              </div>
              <div class="form__input_with_ico">
                <input type="text" class="form__input form__input_with_icon" required="Y" name="PROP[PHONE]" placeholder="<?=GetMessage('DELIVERY_PHONE')?>">
                <img src="/images/px-2daeaa8b.gif" class="ico ico-phone">
              </div>
              <div class="form__select_with_ico">
                <select class="form__select form__select_with_icon" name="PROP[DELIVERY]">
				<?foreach($arResult['PROPERTY_ORDERS']['DELIVERY']['VALUE'] as $arEnum):
					$arVal = explode('&', $arEnum['VALUE']);
				?>
					<option value="<?=$arEnum['ID']?>" data-price="+<?=$arVal['1']?> руб." data-price-value="<?=$arVal['1']?>"><?=$arVal['0']?></option>
				<?endforeach;?>
                </select>
                <img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-delivery">
              </div>
            </td>
            <td>
              <textarea class="form__textarea" name="DETAIL_TEXT" placeholder="<?=GetMessage('DELIVERY_ADRESS')?>"></textarea>
			  <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
			  <input type="submit" name="aaa" value="<?=$arResult["PARAMS_HASH"]?>" class="_hide">
            </td>
          </tr>
        </table>

      </form><!-- /cart__form -->
      <div class="clear"></div>
    </div><!-- /cart -->

    <table class="cart__next">
      <tr>
        <td class="cart__sum">
          <?=GetMessage('TOTAL_SUM')?>: <span class="cart__total"><?=$arResult['SUM_TOTAL_CURRENCY']?></span>
        </td>
        <td class="right">
          <a href="/success" class="button button_large" id="delivery_submit"><?=GetMessage('DELIVERY_SBT')?></a>
        </td>
      </tr>
    </table>