<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<div class="module__title">
    <span class="module__caption <?=$arResult['STEP'] == 0 ? '' : 'module__caption_done'?>"><?=GetMessage('BUY_TITLE')?></span>
    <span class="module__caption <?=$arResult['STEP'] == 0 ? 'module__caption_notready' : ($arResult['STEP'] == 1 ? '' : 'module__caption_done')?>">
      <img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-step-next">
      <span><?=GetMessage('DELIVERY_TITLE')?></span>
	<?if($arResult['STEP'] > 0):?>
	</span>
	<span class="module__caption <?=$arResult['STEP'] < 2? 'module__caption_notready' : ''?>">
	<?endif;?>
      <img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-step-next">
      <span><?=GetMessage('READY_TITLE')?></span>
    </span>
    <span class="module__actions"></span>
  </div><!-- /module__title -->