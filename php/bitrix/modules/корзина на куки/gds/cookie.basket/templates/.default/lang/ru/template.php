<?
$MESS['BUY_TITLE'] = 'Покупки';
$MESS['DELIVERY_TITLE'] = 'Доставка';
$MESS['READY_TITLE'] = 'Готово';
$MESS['DELETE_BTN'] = 'Удалить из корзины';
$MESS['BASE_SIM'] = 'Обычная Sim';
$MESS['MICRO_SIM'] = 'MicroSim';
$MESS['NANO_SIM'] = 'NanoSim';
$MESS['NOTIF_PHONE'] = 'Не забудьте выбрать<br>номер к тарифу';
$MESS['TOTAL_SUM'] = 'Общая сумма';
$MESS['DELIVERY_TYPE'] = 'Тип доставки';
$MESS['NOT_ITEMS'] = 'Ваша корзина пуста';

$MESS['DELIVERY_NAME'] = 'Ваше имя и фамилия';
$MESS['DELIVERY_PHONE'] = 'Ваш номер телефона';
$MESS['DELIVERY_ADRESS'] = 'Укажите адрес доставки';
$MESS['DELIVERY_SBT'] = 'Готово';

$MESS['MESSAGE_OK'] = 'В ближайшее время наш менеджер свяжется с вами.';
$MESS['SUCCESS_ORDER'] = 'ПОЗДРАВЛЯЕМ!<br>ВАШ ЗАКАЗ ОФОРМЛЕН!'; //№#ORDER_ID# 
$MESS['MAIN_TITLE'] = 'На главную';