<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<div class="cart cart_success">

      <div class="cart__image">
        <img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-success">
      </div>

      <div class="cart__message">
        <div><?=GetMessage('SUCCESS_ORDER'/*, array('#ORDER_ID#' => $arResult['ORDER_ID'])*/)?></div>
        <p><?=GetMessage('MESSAGE_OK')?></p>
        <a href="<?=SITE_DIR?>" class="button button_large"><?=GetMessage('MAIN_TITLE')?></a>
      </div>


      <div class="clear"></div>
    </div><!-- /cart -->