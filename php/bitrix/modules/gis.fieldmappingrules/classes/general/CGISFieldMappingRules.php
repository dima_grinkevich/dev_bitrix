<?php

class CGISFieldMappingRules
{
    const TABLE = 'b_gis_fieldmappingrules';
    const MODULE_ID = 'gis.fieldmappingrules';

    // allowed types: checkbox, radio, select, multiselect

    public static $modules = array(
        'deal' => 'CRM_DEAL',
        'lead' => 'CRM_LEAD',
    );

    public static function getTypeByFieldSettings($multiple, $display)
    {
        if ($display == 'LIST' && $multiple == 'N') return 'select';
        if ($display == 'LIST' && $multiple == 'Y') return 'multiselect';
        if ($display == 'CHECKBOX' && $multiple == 'N') return 'radio';
        if ($display == 'CHECKBOX' && $multiple == 'Y') return 'checkbox';
    }

    public static function GetRules($moduleName)
    {
        global $USER_FIELD_MANAGER;
        global $DB;

        $strSql = 'SELECT * FROM `' . CGISFieldMappingRules::TABLE . '` WHERE MODULE_ID = "' . self::$modules[$moduleName] . '"';
        $savedData = $DB->Query($strSql, false);
        $dbRow = $savedData->Fetch();
        $savedData = unserialize($dbRow['VALUE']);

        $allModuleFields = $USER_FIELD_MANAGER->GetUserFields(self::$modules[$moduleName], 0, LANGUAGE_ID);

        $allFields = array();
        foreach ($allModuleFields as $k => $v) {
			
			$t = ord($v['VALUE']);
			$char = in_array($t, array(226, 239));
			if($char)
				$v['VALUE'] = 'да';
            $allFields[$v['ID']] = $v;
        }

        $allFieldNames = array();
        foreach ($allModuleFields as $k => $v) {
            $allFieldNames[$v['FIELD_NAME']] = $v;
        }

        $savedDataArray = explode("\r\n", $savedData);
        $savedValues = array();
        foreach ($savedDataArray as $row) {
            $data = explode(';', $row);

            if (isset($allFieldNames[$data[0]]) && isset($allFieldNames[$data[2]]) && $data[0] != $data[2]) {
                $savedValues[$allFieldNames[$data[0]]['ID']][$data[1]][] = $allFieldNames[$data[2]]['ID'];
            }
        }

        $mapping = array();
        foreach ($savedValues as $selectId => $values) {
            if (!isset($allFields[$selectId])) continue;
            $field = $allFields[$selectId];

            $map = array(
                'type' => self::getTypeByFieldSettings($field['MULTIPLE'], $field['SETTINGS']['DISPLAY']),
                'is_infoblock' => false,
                'fields' => array(),
                'values' => array(),
				'name' => $allFields[$selectId]['EDIT_FORM_LABEL']
            );
 
            if ($field['USER_TYPE_ID'] == 'enumeration') {
                $options = CUserFieldEnum::GetList(array(), array('USER_FIELD_ID' => $field['ID']));

                foreach ((array)$options->arResult as $res) {
					$t = ord($res['VALUE']);
					$char = in_array($t, array(226, 239));
					if($char)
						$res['VALUE'] = 'да';
                    $map['values'][$res['ID']] = $res['VALUE'];
                }
            }

            if ($field['USER_TYPE_ID'] == 'iblock_element') {
                $map['is_infoblock'] = true;

                $temp = CUserTypeIBlockElement::GetList($field);
                while ($res = $temp->GetNext()) {
                    $map['values'][$res['ID']] = $res['VALUE'];
                }
            }

            foreach ($values as $valueId => $displayFields) {
                foreach ($displayFields as $fieldId) {
                    if (!isset($allFields[$fieldId])) continue;

                    $map['fields'][$valueId][] = $allFields[$fieldId]['FIELD_NAME'];
                    $map['fields_name'][$valueId][] = $allFields[$fieldId]['EDIT_FORM_LABEL'];
					
                }
            }

            $mapping[$allFields[$selectId]['FIELD_NAME']] = $map;
        }
//global $USER; if($_GET['test'] == 'y'){ echo '<pre>'.print_r($mapping,true).'</pre>'; die();}
//        echo '<pre>';print_r($mapping);die();


//        $mapping = array(
//            // select
//            'UF_CRM_1441201113' => array(
//                'type' => 'select',
//                'fields' => array(
//                    '' => array(),
//                    26 => array('UF_CRM_1441178269', 'UF_CRM_1441178665'), // 1 - оба
//                    27 => array('UF_CRM_1441178269'), // 2 - Поле1
//                    28 => array('UF_CRM_1441178665'), // 3 - Новое поле 3
//                )
//            ),
//
//            // radio
//            'UF_CRM_1441204356' => array(
//                'type' => 'radio',
//                'fields' => array(
//                    '' => array('UF_CRM_1441204417'), // поле 1
//                    29 => array('UF_CRM_1441204417', 'UF_CRM_1441204427'), // оба
//                    30 => array('UF_CRM_1441204427'), // поле 2
//                    31 => array(), // ничего
//                )
//            ),
//
//            // checkboxes
//            'UF_CRM_1441270081' => array(
//                'type' => 'checkbox',
//                'fields' => array(
//                    32 => array('UF_CRM_1441270091', 'UF_CRM_1441270100'), // оба
//                    33 => array('UF_CRM_1441270100'), // чекк2
//                    34 => array(), // ничего
//                ),
//            ),
//
//            // multiselect
//            'UF_CRM_1441270174' => array(
//                'type' => 'multiselect',
//                'fields' => array(
//                    35 => array('UF_CRM_1441270186', 'UF_CRM_1441270192'), //оба
//                    36 => array('UF_CRM_1441270192'), // мульти2
//                    37 => array(),
//                ),
//            ),
//        );

        return $mapping;
    }

    public static function frontView()
    {
        global $APPLICATION;

        CJSCore::Init(array('jquery',));
        $APPLICATION->AddHeadScript(SITE_DIR . 'bitrix/js/gis.fieldmappingrules/field_mapping_rules.js');

        $pageUrl = $APPLICATION->GetCurPage();
        $pageItems = explode('/', $pageUrl);

        $allowedModules = array('deal', 'lead');
        if (isset($pageItems[2]) && isset($pageItems[3]) && in_array($pageItems[2], $allowedModules) && ($pageItems[3] == 'edit' || $pageItems[3] == 'show')) {
            $arOptions = CGISFieldMappingRules::GetRules($pageItems[2]);

            $string = '<script type="text/javascript">
                $(document).ready(function () {
                    FieldMappingRules(' . CUtil::PhpToJSObject($arOptions, false, true) . ', "' . $pageItems[3] . '");
                });
            </script>';

            $APPLICATION->AddHeadString($string);
        }
    }
}