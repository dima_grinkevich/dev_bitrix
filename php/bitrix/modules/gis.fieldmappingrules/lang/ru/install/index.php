<?
$MESS['GIS_FMR_MODULE_NAME'] = 'Всплывающие поля';

$MESS['GIS_FMR_MODULE_DESCRIPTION'] = 'Модуль организует всплытие полей в зависимости от выбранных значений в форме';
$MESS['GIS_FMR_PARTNER_NAME'] = 'ИП Гринкевич';
$MESS['GIS_TOOLS_PARTNER_URI'] = '';

$MESS['GIS_FMR_INSTALL_OK'] = 'Установка модуля fieldmappingrules';

$MESS['GIS_FMR_UNINSTALL_OK'] = "Деинсталляция модуля fieldmappingrules";

$MESS['GIS_FMR_INSTALL_STEP1'] = "Модуль fieldmappingrules установлен";
$MESS['GIS_FMR_UNINSTALL_STEP1'] = "Модуль fieldmappingrules успешно удален из системы";
//$MESS['GIS_FMR_'] = '';
