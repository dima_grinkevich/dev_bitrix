<?
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen('/install/index.php'));
include(GetLangFileName($strPath2Lang.'/lang/', '/install/index.php'));

Class gis_fieldmappingrules extends CModule
{
    const MODULE_ID = "gis.fieldmappingrules";
    var $MODULE_ID = "gis.fieldmappingrules";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function __construct()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

		$this->MODULE_NAME = GetMessage('GIS_FMR_MODULE_NAME');
		$this->MODULE_DESCRIPTION = GetMessage('GIS_FMR_MODULE_DESCRIPTION');
		$this->PARTNER_NAME = GetMessage('GIS_FMR_PARTNER_NAME');
		$this->PARTNER_URI = GetMessage("GIS_TOOLS_PARTNER_URI");
    }

    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        // Install events

        CopyDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/gis.fieldmappingrules/install/js', $_SERVER['DOCUMENT_ROOT'].'/bitrix/js', true, true);

        $this->InstallDB();

        RegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile(GetMessage('GIS_FMR_INSTALL_OK'), $DOCUMENT_ROOT . "/bitrix/modules/" . self::MODULE_ID . "/install/step.php");
        return true;
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        UnRegisterModule($this->MODULE_ID);

        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/gis.fieldmappingrules/install/js', $_SERVER['DOCUMENT_ROOT'].'/bitrix/js');

        $this->UnInstallDB();

        $APPLICATION->IncludeAdminFile(GetMessage('GIS_FMR_UNINSTALL_OK'), $DOCUMENT_ROOT . "/bitrix/modules/" . self::MODULE_ID . "/install/unstep.php");
        return true;
    }

    function InstallDB()
	{
		global $DB, $APPLICATION, $USER;

		$this->errors = false;
		if (!$DB->Query("SELECT 'x' FROM b_gis_fieldmappingrules", true))
		{
			$this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/gis.fieldmappingrules/install/db/'.strtolower($DB->type).'/install.sql');
        }

        RegisterModuleDependences('main', 'OnPageStart', 'gis.fieldmappingrules', 'CGISFieldMappingRules', 'frontView');

        if (is_array($this->errors))
		{
			$GLOBALS['errors'] = $this->errors;
			$APPLICATION->ThrowException(implode(' ', $this->errors));
			return false;
		}

		return true;
    }

    function UnInstallDB()
	{
		global $DB, $APPLICATION, $USER;

		$this->errors = false;
		$this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/gis.fieldmappingrules/install/db/'.strtolower($DB->type).'/uninstall.sql');

        UnRegisterModuleDependences('main', 'OnPageStart', 'gis.fieldmappingrules', 'CGISFieldMappingRules', 'frontView');

        if (is_array($this->errors))
		{
			$GLOBALS['errors'] = $this->errors;
			$APPLICATION->ThrowException(implode(' ', $this->errors));
			return false;
		}

		return true;
    }
}
