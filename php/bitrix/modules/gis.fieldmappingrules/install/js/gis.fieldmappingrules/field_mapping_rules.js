var FieldMappingRules = function (rules, action) {
    this.rules = rules;
    this.action = action;
    this.viewedFields = [];
    var $this = this;

    /**
     * Create selector for get object
     *
     * @param fieldType type of field (radio, checkbox, multiselect, select)
     * @param name text name of field without "[]"
     * @returns {string}
     */
    this.buildSelector = function (fieldType, name, isInfoBlock) {
        if (isInfoBlock == true) return '[name^="' + name + '"]';
        if (fieldType == 'radio') return 'input[name="' + name + '"]';
        if (fieldType == 'checkbox') return 'input[name="' + name + '[]"]';
        if (fieldType == 'multiselect') return 'select[name="' + name + '[]"]';
        if (fieldType == 'select') return 'select[name="' + name + '"]';

        return 'select[name="' + name + '"]';
    };

    /**
     * Get selected values of fields
     * @param fieldType type of field (radio, checkbox, multiselect, select)
     * @param name text name of field without "[]"
     * @returns {Array}
     */
    this.getFieldValues = function (fieldType, name, isInfoBlock) {
        ids = [];

        if ($this.action == 'edit') {
            selector = this.buildSelector(fieldType, name, isInfoBlock);

            if (!isInfoBlock) {
                if (fieldType == 'radio') ids.push($(selector + ':checked').val());
                if (fieldType == 'checkbox') {
                    $(selector + ':checked').each(function () {
                        ids.push(this.value);
                    });
                }
                if (fieldType == 'multiselect') ids = $(selector).val();
                if (fieldType == 'select') ids.push($(selector).val());
            } else {
                $(selector).each(function () {
                    ids.push(this.value);
                });
            }
        }

        if ($this.action == 'show') {
            values = [];

            $('[data-dragdrop-id="' + name + '"]').find('.fields').each(function() {
                value = $(this).text();
                if (value) values.push(value);
            });

            if ($this.rules[name] != undefined && $this.rules[name].values != undefined) {
                for (k in $this.rules[name].values) {
                    for (j in values) {
                        if ($this.rules[name].values[k] == values[j]) ids.push(k);
                    }
                }
            }
        }
console.log(name);
console.log(ids);
        return ids;
    };

    /**
     * Bind each field for change action
     * @param obj
     */
    this.onChangeFunction = function (obj, originalName) {
        originalName = originalName || $(obj).attr('name');
        nameInArray = originalName.replace(/\[[0-9]\]/gi, '').replace('[]', '');

        selectedValues = $this.getFieldValues($this.rules[nameInArray].type, nameInArray, $this.rules[nameInArray].is_infoblock);

        // view by each rule
        for (j in $this.rules[nameInArray].fields) {
            dependentFields = $this.rules[nameInArray].fields[j];

            // hide all fields
            for (i in dependentFields) {
//console.log('hide - [data-dragdrop-id="' + $this.viewedFields[i] + '"]');
                $('[data-dragdrop-id="' + dependentFields[i] + '"]').hide();
            }
        }

        // view by each rule
        for (l in selectedValues) {
            dependentFields = $this.rules[nameInArray].fields[selectedValues[l]];

            // hide all fields
            for (i in dependentFields) {
                if ($this.viewedFields.length > 0) {
                    $this.viewedFields.push(dependentFields[i]);
                }

//console.log('show2 - [data-dragdrop-id="' + $this.viewedFields[i] + '"]');
                $('[data-dragdrop-id="' + dependentFields[i] + '"]').show();
            }
        }
    };

    /**
     * Initial function - constructor
     */
    this.init = function () {
        $this.viewedFields.push('initField');

        for (k in this.rules) {
            switch (this.action) {
                // edit page
                case 'edit':
                    if($('tr[data-dragdrop-id=' + k + '] span.fields').length > 0){
                        $this.onChangeFunction($('tr[data-dragdrop-id=' + k + ']'), k);
                    }else{
                        selector = this.buildSelector(this.rules[k].type, k, this.rules[k].is_infoblock);

                        $(document).on('change', selector, function(){
                            $this.onChangeFunction(this);
                        } );

                        $(selector).trigger('change');
                        break;
                    }
                // show page
                case 'show':
                    $this.onChangeFunction($('tr[data-dragdrop-id=' + k + ']'), k);
                    break;
            }
        }

        for (i in $this.viewedFields) {
//console.log('show - [data-dragdrop-id="' + $this.viewedFields[i] + '"]');
            $('[data-dragdrop-id="' + $this.viewedFields[i] + '"]').show();
        }

        $this.viewedFields = [];
    };

    // init function
    this.init();
};
