<?php
global $DBType;

$arClasses = array(
    'CGISFieldMappingRules' => 'classes/general/CGISFieldMappingRules.php'
);

CModule::AddAutoloadClasses("gis.fieldmappingrules", $arClasses);
