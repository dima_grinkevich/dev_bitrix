<h1>Настройка модуля</h1>

<?
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
include_once($GLOBALS["DOCUMENT_ROOT"] . "/bitrix/modules/gis.fieldmappingrules/classes/general/CGISFieldMappingRules.php");

$module_id = "gis.fieldmappingrules";
$RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($RIGHT >= "R") :
///// Читаем данные и формируем для вывода
    global $USER_FIELD_MANAGER;
    global $DB;

    $allowedModules = array('CRM_DEAL', 'CRM_LEAD'); // set allowed modules

    $userFields = array();
    $aTabs = array();
    foreach ($allowedModules as $k => $entity_id) {
        $allModuleFields = $USER_FIELD_MANAGER->GetUserFields($entity_id, 0, LANGUAGE_ID);

        $fields = array();
        $allFields = array();

        if ($allModuleFields) {
            $fields['selects'] = array();
            $fields['other_fields'] = array();
            foreach ($allModuleFields as $fId => $fData) {
                if ($fData['USER_TYPE_ID'] == 'enumeration') {
                    $options = CUserFieldEnum::GetList(array(), array('USER_FIELD_ID' => $fData['ID']));
                    $fData['values'] = $options->arResult;

                    $fields['selects'][] = $fData;
                }

                if ($fData['USER_TYPE_ID'] == 'iblock_element') {
                    $temp = CUserTypeIBlockElement::GetList($fData);
                    while ($res = $temp->GetNext()) {
                        $fData['values'][] = $res;
                    }

                    $fields['selects'][] = $fData;
                }

                $fields['other_fields'][] = $fData;

                if ($fData['values']) {
                    $fValues = array();
                    foreach ($fData['values'] as $fValue) {
						$t = ord($fValue['VALUE']);
						$char = in_array($t, array(226, 239));
						if($char)
							$fValue['VALUE'] = 'да';
                        $fValues[$fValue['ID']] = $fValue;
                    }

                    $fData['values'] = $fValues;
                }
                $allFields[$fData['FIELD_NAME']] = $fData;
            }
        }

        $userFields[$entity_id]['fields'] = $fields;
        $userFields[$entity_id]['all_fields'] = $allFields;

        $aTabs[] = array(
            'DIV' => 'edit' . ($k + 1),
            'TAB' => GetMessage("MAIN_TAB_SET") . ' ' . $entity_id,
            'ICON' => "perfmon_settings",
            'TITLE' => GetMessage("MAIN_TAB_TITLE_SET") . ' ' . $entity_id,

        );
    }

    $tabControl = new CAdminTabControl("tabControl", $aTabs);
	$delimeterPrompt = '#';
	
    if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && $RIGHT == "W" && check_bitrix_sessid()) {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/perfmon/prolog.php");

        if (strlen($RestoreDefaults) > 0) {
            foreach ($allowedModules as $entity_id) {
                $sql = 'DELETE FROM `' . CGISFieldMappingRules::TABLE . '` WHERE MODULE_ID = "' . $DB->ForSql($entity_id) . '"';
                $DB->Query($sql);

                $DB->Insert(CGISFieldMappingRules::TABLE, array(
                    'MODULE_ID' => '"' . $DB->ForSql($entity_id) . '"',
                    'VALUE' => '""',
                ));
            }
        } else {
            foreach ($_REQUEST['show'] as $entity_id => $values) {
                $rows = explode("\r\n", $values);
                foreach ($rows as $k => $row) {
                    $data = explode($delimeterPrompt, $row);
                    $rows[$k] = trim($data[0]);
                }
                $values = implode("\r\n", $rows);

                $sql = 'DELETE FROM `' . CGISFieldMappingRules::TABLE . '` WHERE MODULE_ID = "' . $DB->ForSql($entity_id) . '"';
                $DB->Query($sql);

                $val = serialize($values);
                $DB->Insert(CGISFieldMappingRules::TABLE, array(
                    'MODULE_ID' => '"' . $DB->ForSql($entity_id) . '"',
                    'VALUE' => '"' . $DB->ForSql($val) . '"'
                ));
            }
        }

        ob_start();
        $Update = $Update . $Apply;
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php");
        ob_end_clean();
    }


    ?>
    <form method="post"
          action="<? echo $APPLICATION->GetCurPage()?>?mid=<?= urlencode($module_id) ?>&amp;lang=<?= LANGUAGE_ID ?>">
        <?
        $tabControl->Begin();

        foreach ($userFields as $entity_id => $data) {
            $tabControl->BeginNextTab();

            $strSql = 'SELECT * FROM `' . CGISFieldMappingRules::TABLE . '` WHERE MODULE_ID = "' . $DB->ForSql($entity_id) . '"';
            $savedData = $DB->Query($strSql, false);
            $dbRow = $savedData->Fetch();
            $savedValues = unserialize($dbRow['VALUE']);

            if (!$data['fields']) {
                echo '<p>Полей не найдено</p>';
                continue;
            }

//            echo '<pre>';print_r($data['all_fields']['UF_WRITE_COST_MANUAL']);die();

            $savedLines = explode("\r\n", $savedValues);
            $fieldsData = array();
            foreach ($savedLines as $k => $row) {
                $arr = explode(';', $row);

                $oneElement = isset($data['all_fields'][$arr[0]]) ? $data['all_fields'][$arr[0]]['LIST_COLUMN_LABEL'] : '???';
                $twoElement = isset($data['all_fields'][$arr[0]]['values'][$arr[1]]) ? $data['all_fields'][$arr[0]]['values'][$arr[1]]['VALUE'] : '???';
                $treeElement = isset($data['all_fields'][$arr[2]]) ? $data['all_fields'][$arr[2]]['LIST_COLUMN_LABEL'] : '???';

                $row = $row . '      ' . $delimeterPrompt . $oneElement . ';' . $twoElement . ';' . $treeElement;

                $savedLines[$k] = $row;
            }

            $savedValues = implode("\r\n", $savedLines);

            ?>

            <tr>
                <th style="text-align:left; vertical-align: top;">Поле;ID Значения Поля;Показывать поля при выборе
                    значения
                </th>
            </tr>
            <tr>
                <td>
                    <textarea name="show[<? echo $entity_id; ?>]"
                              style="width:100%; height: 200px;"><?php echo $savedValues; ?></textarea>
                </td>
            </tr>


        <?
        }

        ?>
        <? $tabControl->Buttons();?>
        <input <? if ($RIGHT < "W") echo "disabled" ?> type="submit" name="Update"
                                                       value="<?= GetMessage("MAIN_SAVE") ?>"
                                                       title="<?= GetMessage("MAIN_OPT_SAVE_TITLE") ?>"
                                                       class="adm-btn-save">
        <input <? if ($RIGHT < "W") echo "disabled" ?> type="submit" name="Apply"
                                                       value="<?= GetMessage("MAIN_OPT_APPLY") ?>"
                                                       title="<?= GetMessage("MAIN_OPT_APPLY_TITLE") ?>">
        <? if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
            <input <? if ($RIGHT < "W") echo "disabled" ?> type="button" name="Cancel"
                                                           value="<?= GetMessage("MAIN_OPT_CANCEL") ?>"
                                                           title="<?= GetMessage("MAIN_OPT_CANCEL_TITLE") ?>"
                                                           onclick="window.location='<? echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
            <input type="hidden" name="back_url_settings"
                   value="<?= htmlspecialcharsbx($_REQUEST["back_url_settings"]) ?>">
        <? endif?>
        <input type="submit" name="RestoreDefaults" title="<? echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>"
               OnClick="confirm('<? echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>')"
               value="<? echo GetMessage("MAIN_RESTORE_DEFAULTS")?>">
        <?= bitrix_sessid_post(); ?>
        <? $tabControl->End();?>
    </form>
    <?
    if (!empty($arNotes)) {
        echo BeginNote();
        foreach ($arNotes as $i => $str) {
            ?><span class="required"><sup><?echo $i + 1 ?></sup></span><?echo $str ?><br><?
        }
        echo EndNote();
    }
    ?>
<? endif; ?>