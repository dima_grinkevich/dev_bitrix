<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/cat/([a-zA-Z0-9\\-\\_]+)/([a-zA-Z0-9\\-\\_]+)/\\??.*\$#",
		"RULE" => "SECT_CODE=\$1&PROD_CODE=\$2",
		"ID" => "",
		"PATH" => "/cat/detail.php",
	),
	array(
		"CONDITION" => "#^/useful/articles/([a-zA-Z0-9\\-\\_]+)/\\??.*\$#",
		"RULE" => "ARTICLE_CODE=\$1",
		"ID" => "",
		"PATH" => "/useful/articles/detail.php",
	),
	array(
		"CONDITION" => "#^/contacts/([a-zA-Z0-9\\-\\_]+)/\\??.*\$#",
		"RULE" => "OFFICE_CODE=\$1",
		"ID" => "",
		"PATH" => "/contacts/detail.php",
	),
	array(
		"CONDITION" => "#^/sale/([a-zA-Z0-9\\-\\_]+)/\\??.*\$#",
		"RULE" => "ACTION_CODE=\$1",
		"ID" => "",
		"PATH" => "/sale/detail.php",
	),
	array(
		"CONDITION" => "#^/news/([a-zA-Z0-9\\-\\_]+)/\\??.*\$#",
		"RULE" => "NEWS_CODE=\$1",
		"ID" => "",
		"PATH" => "/news/detail.php",
	),
	array(
		"CONDITION" => "#^/cat/([a-zA-Z0-9\\-\\_]+)/\\??.*\$#",
		"RULE" => "SECT_CODE=\$1",
		"ID" => "",
		"PATH" => "/cat/index.php",
	),
);

?>