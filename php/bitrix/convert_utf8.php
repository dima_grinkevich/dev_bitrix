<?
error_reporting(E_ALL & ~E_NOTICE);

if(version_compare(phpversion(), '5.0.0') == -1)
	die('PHP 5.0.0 or higher is required!');

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
header("Content-type: text/html; charset=cp1251");
echo '<html><head><title>����������� ����� � cp1251</title></head><body>';

$STEP = intval($_REQUEST['step']);
if (!$STEP)
	$STEP = 1;
$strRes = '';
define('LIMIT', 10); // time limit

if (!$_REQUEST['go'])
{
	// ������������ ��������
	if (!function_exists('mb_convert_encoding'))
		Error('�� �������� ���������� mbstring');

	if (ini_get('mbstring.func_overload') != 2)
		Error('�������� ��������� mbstring.func_overload �� ����� 2');


	if (!defined('BX_UTF') || BX_UTF !== true)
		Error('��������� BX_UTF �� ���������� � true � /bitrix/php_interface/dbconn.php: <br><br><i>define("BX_UTF", true);</i>');

	$res = $DB->Query('SHOW VARIABLES LIKE "character_set_results"');
	$f = $res->Fetch();
	if (strtolower($f['Value']) != 'cp1251')
		Error('���� ������ �������� � ���������, �������� �� cp1251 (��������: '.$f['Value'].')');
}
else // GO!
{
	define('START_TIME', time()); // �������� ����� ������

	if ($STEP < 3)
	{
		define('START_PATH', $_SERVER['DOCUMENT_ROOT']); // ��������� ����� ��� ������
		if ($_REQUEST['break_point']) 
			define('SKIP_PATH',$_REQUEST['break_point']); // ������������� ����

		Search(START_PATH);
		if (defined('BREAK_POINT'))
		{
			?><form method=post id=postform>
				<input type=hidden name=step value="<?=$STEP?>">
				<input type=hidden name=go value=Y>
				<input type=hidden name=break_point value="<?=htmlspecialchars(BREAK_POINT)?>">
			</form>
			��� ���������...<br>
			������� ����: <i><?=htmlspecialchars(BREAK_POINT)?></i>
			<script>window.setTimeout("document.getElementById('postform').submit()",500);</script><? // ������� ����� ������� ������� �����
			die();
		}
		else
		{
			$strRes = "<font color=green><b>��� $STEP �������� �������</b></font>";
			$STEP++;
		}
	}
}

$arStep = array(
	0,
	'�������� ���� �������',
	'����������� ������ � cp1251',
	//'����������� ���� ������ � cp1251'
)

?>
<h1>����������� ����� � cp1251</h1>
<p>������ �������, ���������� � �����: <a href="http://dev.1c-bitrix.ru/community/blogs/howto/1466.php">http://dev.1c-bitrix.ru/community/blogs/howto/1466.php</a></p>
<?=$strRes?>
<form method=post>
	<h2>��� <?=$STEP?></h2>
	<input type=hidden name=step value="<?=$STEP?>">
	<input type=submit name=go value="<?=$arStep[$STEP]?>">
	<?=($STEP == 1 ? '<input type=button onclick="document.location=\'?step=3\'" value="������� � ����������� ����">' : '' )?>
</form>
<?

function Search($path)
{
	if (time() - START_TIME > LIMIT)
	{
		if (!defined('BREAK_POINT'))
			define('BREAK_POINT', $path);
		return;
	}

	if (defined('SKIP_PATH') && !defined('FOUND')) // ��������, ������� �� ������� ����
	{
		if (0!==strpos(SKIP_PATH, dirname($path))) // ����������� ��� ��� ��� ���� 
			return;

		if (SKIP_PATH==$path) // ���� ������, ���������� ������ �����
			define('FOUND',true);
	}

	if (is_dir($path)) // dir
	{
		$dir = opendir($path);
		while($item = readdir($dir))
		{
			if ($item == '.' || $item == '..')
				continue;

			Search($path.'/'.$item);
		}
		closedir($dir);
	}
	else // file
	{
		if (!defined('SKIP_PATH') || defined('FOUND'))
		{
			if ((substr($path,-3) == '.js' || substr($path,-4) == '.php' || basename($path) == 'trigram') && $path != __FILE__)
				Process($path);
		}
	}
}

function Process($file)
{
	global $STEP;

	if ($STEP == 1)
	{
		if (!is_writable($file))
			Error('���� �� �������� �� ������: '.$file);
	}
	elseif ($STEP == 2)
	{
		$content = file_get_contents($file);

		/*if (GetStringCharset($content) != 'utf8')
			return;*/

		if ($content === false)
			Error('�� ������� ��������� ����: '.$file);

		if (file_put_contents($file, mb_convert_encoding($content, 'cp1251', 'utf8')) === false)
			Error('�� ������� ��������� ����: '.$file);
		
	}
}

function GetStringCharset($str)
{ 
	global $APPLICATION;
	if (preg_match("/[\xe0\xe1\xe3-\xff]/",$str))
		return 'cp1251';
	$str0 = $APPLICATION->ConvertCharset($str, 'cp1251', 'cp1251');
	if (preg_match("/[\xe0\xe1\xe3-\xff]/",$str0,$regs))
		return 'cp1251';
	return 'ascii';
}

function Error($text)
{
	die('<font color=red>'.$text.'</font>');
}
?>
