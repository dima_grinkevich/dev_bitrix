<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

CPageTemplate::IncludeLangFile(__FILE__);

class CDBCatalogPageTemplate
{
	function GetDescription()
	{
		return array(
			"name"=>GetMessage("db_catalog_template_name"), 
			"description"=>GetMessage("db_catalog_template_desc"),
			"icon"=>"/bitrix/themes/.default/public/panel_new/menus/create_template.gif",
			//"modules"=>array("forum"),
		);
	}
	
	function GetFormHtml()
	{
		
		if(!CModule::IncludeModule('iblock'))
			return '';


		$s = '
		<tr class="section">
			<td colspan="2">'.GetMessage("db_catalog_template_settings").'</td>
		</tr>
		<tr>
			<td class="bx-popup-label">'.GetMessage("db_catalog_template_demo").'</td>
			<td><select name="dbiblcokdemo">';
			$res = CIBlock::GetList(
				Array(), 
				Array(
				), false
			);
			while($ar_res = $res->Fetch())
			{
				$s .= '<option value="'.$ar_res['ID'].'" '.($ar_res['CODE'] == 'DEMO' ? 'selected="selected"' : '').'>'.$ar_res['NAME'].' ['.$ar_res['CODE'].']'.'</option>';
			}					
			$s .= '
			</select></td>
		</tr>
		';	
		
		$s .= '
		<tr>
			<td class="bx-popup-label">'.GetMessage("db_catalog_template_type").'</td>
			<td><select name="dbiblcoktype">';
			$db_iblock_type = CIBlockType::GetList();
			while($ar_res = $db_iblock_type->Fetch())
			{
				$s .= '<option value="'.$ar_res['ID'].'">'.$ar_res['ID'].'</option>';
			}	
			$s .= '
			</select></td>
		</tr>
		';	
		$s .= '
		<tr>
			<td class="bx-popup-label">'.GetMessage("db_catalog_template_isFilter").'</td>
			<td><select name="dbisFiletr">
				<option value="Y">Y</option>
				<option value="N" selected="selected">N</option>
			</select></td>
		</tr>
		';	
		$s .= '
		<tr>
			<td class="bx-popup-label">'.GetMessage("db_catalog_template_iblockTitle").'</td>
			<td><input type="text" style="width:90%;" id="dbiblcoktitle" name="dbiblcoktitle" value=""></td>
		</tr>
		';
		return $s;
	}
	
	function CopyIBlock($ID, $type, $checkUnique = false, $arDop = array()) 
    { 
		CModule::IncludeModule('iblock');
		
        $res = CIBlock::GetByID($ID)->Fetch(); 
		
        $res["IBLOCK_TYPE_ID"] = $type; 
		
        if (is_set($res, "PICTURE") && intval($res["PICTURE"]) > 0) 
            $res["PICTURE"] = CFile::MakeFileArray($res["PICTURE"]); 

        if ($checkUnique) { 
            $res["EXTERNAL_ID"] = 'copy_' . $res["ID"]; 

            $rsIBlock = CIBlock::GetList(array(), array("TYPE" => $res["IBLOCK_TYPE_ID"], "=XML_ID" => $res["EXTERNAL_ID"])); 
            if ($arIBlock = $rsIBlock->Fetch()) 
                return $arIBlock["ID"]; 
        } 
		if(count($arDop) > 0){
			foreach($arDop as $key => $arNew){
				$res[$key] = $arNew;
			}
		}
        $ib = new CIBlock(); 
        if ($NEW_ID = $ib->Add($res)) { 
            CIBlock::SetFields($NEW_ID, CIBlock::GetFields($ID)); 
            CIBlock::SetPermission($NEW_ID, CIBlock::GetGroupPermissions($ID)); 

            $arProperties = $arPropertyEnums = $arUFProperties = $arUFPropertyEnums = array(); 

            self::syncIblockCatalog($ID, $NEW_ID); 
            self::syncIblockProperties($ID, $NEW_ID, $arProperties, $arPropertyEnums, $arUFProperties, $arUFPropertyEnums); 
            self::syncIblockPropertiesUserSettings($ID, $NEW_ID, $arProperties, $arUFProperties);
        } 
        return $NEW_ID; 
    } 
	function syncIblockCatalog($FROM_IBLOCK_ID, $TO_IBLOCK_ID) 
    { 
        if (!CModule::IncludeModule("iblock")) { 
            $GLOBALS["APPLICATION"]->ThrowException("CAT_ERROR_IBLOCK_NOT_INSTALLED"); 
            return false; 
        } 

        if (CModule::IncludeModule("catalog") && CCatalog::GetByID($FROM_IBLOCK_ID)) { 
            CCatalog::Add(array("IBLOCK_ID" => $TO_IBLOCK_ID, "YANDEX_EXPORT" => "N", "SUBSCRIPTION" => "N")); 
        } 
    } 
	function syncIblockProperties($FROM_IBLOCK_ID, $TO_IBLOCK_ID, &$arProperties = array(), &$arPropertyEnums = array(), &$arUFProperties = array(), &$arUFPropertyEnums = array()) 
    { 
        if (!CModule::IncludeModule("iblock")) { 
            $GLOBALS["APPLICATION"]->ThrowException("CAT_ERROR_IBLOCK_NOT_INSTALLED"); 
            return false; 
        } 

        if (!is_array($arUFPropertyEnums)) 
            $arUFPropertyEnums = array(); 


        $obUserField = new CUserTypeEntity; 
        $obEnum = new CUserFieldEnum; 

        $arFilter = array( 
            "ENTITY_ID" => "IBLOCK_" . $FROM_IBLOCK_ID . "_SECTION", 
        ); 
        $rsData = CUserTypeEntity::GetList(array(), $arFilter); 
        while ($resData = $rsData->Fetch()) { 

            $UF_ID = $resData["ID"]; 
            $resData = CUserTypeEntity::GetByID($UF_ID); 

            if ($resData["USER_TYPE_ID"] == "enumeration") { 
                $rsEnum = CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => $resData["ID"])); 
                $enum = 0; 
                while ($resEnum = $rsEnum->Fetch()) { 
                    $resData["ENUM"]["n" . $enum++] = $resEnum; 
                } 
            } 

            $resData["ENTITY_ID"] = "IBLOCK_" . $TO_IBLOCK_ID . "_SECTION"; 

            $rsDataExists = CUserTypeEntity::GetList(array(), array("ENTITY_ID" => $resData["ENTITY_ID"], 
                    "FIELD_NAME" => $resData["FIELD_NAME"],) 
            ); 

            if ($resDataExists = $rsDataExists->Fetch()) { 

                $NEW_ID = $resDataExists["ID"]; 
                unset($resData["ID"]); 

                $obUserField->Update($NEW_ID, $resData); 
                if (!empty($resData["ENUM"])) { 

                    foreach ($resData["ENUM"] as $k => $v) { 
                        unset($resData["ENUM"][$k]["USER_FIELD_ID"]); 
                        unset($resData["ENUM"][$k]["ID"]); 
                    } 

                    $obEnum->SetEnumValues($NEW_ID, $resData["ENUM"]); 
                } 
            } else { 
                unset($resData["ID"]); 
                $NEW_ID = $obUserField->Add($resData); 

                if (!empty($resData["ENUM"])) { 
                    foreach ($resData["ENUM"] as $k => $v) { 
                        unset($resData["ENUM"][$k]["USER_FIELD_ID"]); 
                        unset($resData["ENUM"][$k]["ID"]); 
                    } 

                    $obEnum->SetEnumValues($NEW_ID, $resData["ENUM"]); 
                } 
            } 

            $resEnumsOld = $resEnumsNew = array(); 
            $rsEnums = $obEnum->GetList(array(), array("USER_FIELD_ID" => $UF_ID)); 
            while ($resEnums = $rsEnums->Fetch()) { 
                $resEnumsOld[] = $resEnums["ID"]; 
            } 

            $rsEnums = $obEnum->GetList(array(), array("USER_FIELD_ID" => $NEW_ID)); 
            while ($resEnums = $rsEnums->Fetch()) { 
                $resEnumsNew[] = $resEnums["ID"]; 
            } 

            if (count($resEnumsOld) == count($resEnumsOld) && !empty($resEnumsOld)) { 
                $arUFPropertyEnums += array_combine($resEnumsOld, $resEnumsNew); 
            } 

            $arUFProperties[$UF_ID] = $NEW_ID; 

        } 

        $arUpdateProperties = array(); 
        $rsProperty = CIBlockProperty::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $FROM_IBLOCK_ID)); 
        while ($arProperty = $rsProperty->Fetch()) { 
            $arUpdateProperties[] = $arProperty; 
        } 

        foreach ($arUpdateProperties as $arProperty) { 

            $arProperty["IBLOCK_ID"] = $TO_IBLOCK_ID; 
            $arProperty["XML_ID"] = "PROP_" . $arProperty["ID"]; 

            $ibp = new CIBlockProperty; 
            $rs = CIBlockProperty::GetList(array(), array("IBLOCK_ID" => $arProperty["IBLOCK_ID"], "XML_ID" => $arProperty["XML_ID"])); 
            if ($ar = $rs->Fetch()) { 
                $ID = $ar["ID"]; 
                $ibp->Update($ar["ID"], $arProperty); 
                $arProperties[$arProperty["ID"]] = $ar["ID"]; 
            } else { 
                if (!($ID = $ibp->Add($arProperty))) { 
                    $GLOBALS["APPLICATION"]->ThrowException("DEFATOOLS_IB_DEMO_PROP_CREATE_ERR" . ': ' . $ibp->LAST_ERROR); 
                } 
                $arProperties[$arProperty["ID"]] = $ID; 
            } 

            $arUpdateEnums = array(); 
            if ($arProperty["PROPERTY_TYPE"] == 'L') { 
                if ($ID) { 
                    $rs = CIBlockPropertyEnum::GetList(array("SORT" => "ASC"), array("PROPERTY_ID" => $arProperty["ID"])); 
                    while ($ar = $rs->Fetch()) { 

                        $ar["IBLOCK_ID"] = $arProperty["IBLOCK_ID"]; 
                        $ar["PROPERTY_ID"] = $ID; 
//                                    $ar["XML_ID"] = "ENUM_".$ar["ID"]; 
                        $arUpdateEnums[] = $ar; 
                    } 
                } 
            } 

            foreach ($arUpdateEnums as $arEnum) { 
                $ibpenum = new CIBlockPropertyEnum; 

                $rs = CIBlockPropertyEnum::GetList(array(), array("PROPERTY_ID" => $ID, "XML_ID" => $arEnum["XML_ID"])); 
                if ($ar = $rs->Fetch()) { 
                    $arPropertyEnums[$arEnum["ID"]] = $ar["ID"]; 
                    unset($arEnum["ID"]); 
                    $ibpenum->Update($ar["ID"], $arEnum); 
                } else { 
                    $arEnumID = $arEnum["ID"]; 
                    unset($arEnum["ID"]); 
                    if (!($ENUM_ID = $ibpenum->Add($arEnum))) { 
                        $GLOBALS["APPLICATION"]->ThrowException(("DEFATOOLS_IB_DEMO_PROP_ADD_LTYPE_ERR")); 
                    } 
                    $arPropertyEnums[$arEnumID] = $ENUM_ID; 
                } 
            } 
        } 

    }
	function syncIblockPropertiesUserSettings($FROM_IBLOCK_ID, $TO_IBLOCK_ID, $arProperties = array(), $arUFProperties) 
    { 

        CModule::IncludeModule("iblock"); 

        foreach ($arProperties as $k => $v) { 
            $arPropertiesNew["--PROPERTY_" . $k . "--"] = "--PROPERTY_" . $v . "--"; 
            $arPropertiesNewClear["PROPERTY_" . $k] = "PROPERTY_" . $v; 
        } 

        $iblockHashes = array(); 
        foreach (array($FROM_IBLOCK_ID, $TO_IBLOCK_ID) as $iblock) { 
            $rs = CIBlock::GetById($iblock); 
            $res = $rs->Fetch(); 
            $iblockHashes[$res["ID"]] = "tbl_iblock_list_" . md5($res["IBLOCK_TYPE_ID"] . "." . $res["ID"]); 
        } 

        foreach (array($GLOBALS['USER']->GetID, false) as $user) { 

            foreach (array("section", "element") as $type) { 
                // form 
                $res = CUserOptions::GetOption("form", "form_" . $type . "_" . $FROM_IBLOCK_ID, false, $user); 

                if (!empty($res["tabs"])) { 
                    $res["tabs"] = str_replace(array_keys($arPropertiesNew), array_values($arPropertiesNew), $res["tabs"]); 
                    CUserOptions::SetOption("form", "form_" . $type . "_" . $TO_IBLOCK_ID, $res, ($user === false ? "Y" : "N"), $user); 
                } 
                // /form 
            } 

            // list 
            $res = CUserOptions::GetOption("list", $iblockHashes[$FROM_IBLOCK_ID], false, $user); 

            if ($res["columns"]) { 
                $res["columns"] = explode(",", $res["columns"]); 
                foreach ($res["columns"] as $k => $v) { 
                    if (isset($arPropertiesNewClear[$v])) 
                        $res["columns"][$k] = $arPropertiesNewClear[$v]; 
                } 
                $res["columns"] = implode(",", $res["columns"]); 
            } 

            if (isset($res["by"]) && isset($arPropertiesNewClear[$res["by"]])) 
                $res["by"] = $arPropertiesNewClear[$res["by"]]; 

            CUserOptions::SetOption("list", $iblockHashes[$TO_IBLOCK_ID], $res, ($user === false ? "Y" : "N"), $user); 
            // /list 
        } 

    }
	
	function GetContent($arParams)
	{
		// copy IBLOCK from iblcokdemo to iblcoktype
		$arParams['newiblock'] = self::CopyIBlock(
			intval($_POST['dbiblcokdemo']),
			trim($_POST['dbiblcoktype']),
			false,
			array(
				'NAME' => trim($_POST['dbiblcoktitle']),
				'CODE' => trim($_POST['fileName']),
				'LIST_PAGE_URL' => EscapePHPString($arParams["path"]).'#IBLOCK_CODE#/',
				'SECTION_PAGE_URL' => EscapePHPString($arParams["path"]).'#IBLOCK_CODE#/#SECTION_CODE#/',
				'DETAIL_PAGE_URL' => EscapePHPString($arParams["path"]).'#IBLOCK_CODE#/#SECTION_CODE#/#ELEMENT_CODE#/',
			)
		);
		// creat page
		$s = '
		<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?><?$APPLICATION->IncludeComponent("bitrix:catalog", ".default", array(
	"IBLOCK_TYPE" => "'.$arParams['dbiblcoktype'].'",
	"IBLOCK_ID" => "'.$arParams['newiblock'].'",
	"BASKET_URL" => "/personal/cart/",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"SEF_MODE" => "Y",
	"SEF_FOLDER" => "'.EscapePHPString($arParams["path"]).'",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "Y",
	"USE_FILTER" => "N",
	"USE_COMPARE" => "N",
	"PRICE_CODE" => array(
		0 => "BASE",
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"PRICE_VAT_SHOW_VALUE" => "N",
	"SHOW_TOP_ELEMENTS" => "N",
	"PAGE_ELEMENT_COUNT" => "10",
	"LINE_ELEMENT_COUNT" => "1",
	"ELEMENT_SORT_FIELD" => "sort",
	"ELEMENT_SORT_ORDER" => "asc",
	"LIST_PROPERTY_CODE" => array(
		0 => "FLG_NEW",
		1 => "",
	),
	"INCLUDE_SUBSECTIONS" => "N",
	"LIST_META_KEYWORDS" => "UF_SEO_KW",
	"LIST_META_DESCRIPTION" => "UF_SEO_DESK",
	"LIST_BROWSER_TITLE" => "NAME",
	"PREVIEW_TRUNCATE_LEN" => "250",
	"DETAIL_PROPERTY_CODE" => array(
		0 => "FLG_NEW",
		1 => "",
	),
	"DETAIL_META_KEYWORDS" => "-",
	"DETAIL_META_DESCRIPTION" => "-",
	"DETAIL_BROWSER_TITLE" => "-",
	"USE_SHARE" => "N",
	"LINK_IBLOCK_TYPE" => "",
	"LINK_IBLOCK_ID" => "",
	"LINK_PROPERTY_SID" => "",
	"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
	"USE_ALSO_BUY" => "N",
	"DISPLAY_TOP_PAGER" => "Y",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "������",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => "",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
	"PAGER_SHOW_ALL" => "N",
	"PATH_TO_SHIPPING" => "#SITE_DIR#about/delivery/",
	"DISPLAY_IMG_WIDTH" => "135",
	"DISPLAY_IMG_HEIGHT" => "236",
	"DISPLAY_DETAIL_IMG_WIDTH" => "350",
	"DISPLAY_DETAIL_IMG_HEIGHT" => "1000",
	"DISPLAY_MORE_PHOTO_WIDTH" => "208",
	"DISPLAY_MORE_PHOTO_HEIGHT" => "115",
	"SHARPEN" => "100",
	"EMAIL_TO" => "bitrix@db.by",
	"LIST_TPL" => "'.trim($_POST['dbisFiletr']).'",
	"AJAX_OPTION_ADDITIONAL" => "",
	"SEF_URL_TEMPLATES" => array(
		"sections" => "'.EscapePHPString($arParams["file"]).'",
		"section" => "#SECTION_CODE#/",
		"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
		"compare" => "compare/",
	)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>';
		return $s;
	}
}
$pageTemplate = new CDBCatalogPageTemplate;
?>