<?
AddEventHandler('main', 'OnEpilog', '_Check404Error',1);
function _Check404Error()
{
	global $APPLICATION;
	$arCurDir = explode("/", $APPLICATION->GetCurDir());
   if (defined("ERROR_404") && ERROR_404=="Y")
   {
		foreach($arCurDir as $k => $v){
			if(strlen($v) <=0){
				unset($arCurDir[$k]);
			}
		}
	
		array_pop($arCurDir);
		if(count($arCurDir) <= 0){
			$href = SITE_DIR;
		}else{
			$href = '/'.implode('/', $arCurDir).'/';
		}
		LocalRedirect($href, '', '301 Moved permanently');
   }
}
