<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("CALLBACK_FORM_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("CALLBACK_FORM_COMPONENT_DESCR"),
	"ICON" => "/images/feedback.gif",
	"PATH" => array(
		"ID" => "utility",
	),
);
?>