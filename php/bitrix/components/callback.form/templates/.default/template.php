<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script type="text/javascript" src="/bitrix/js/main/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.min.js"></script>
<script>
    $(function() {
        $("#prop_PHONE").mask("+7 (999)999-99-99");
    });
</script>
<style>
    .callback_form {
        overflow: hidden;
    }
    .callback_form_input {
        text-align: center;
        margin-bottom: 6px;
    }
    .callback_form_input input[type="text"] {
        width: 96%;
        border: 1px solid #999;
    }
    .callback_form_input.error input {
        border: 1px solid red;
    }
    .callback_form_add {
        cursor: pointer;
    }
    .callback_form_submit {
        text-align: center;
    }
    .callback_add_success {
        text-align: center;
        font-size: 20px;
        color: green;
        margin: 10px 0;
    }
    .callback_header {
        text-align: center;
        font-size: 14px;
        margin: 0 0 6px 0;
    }
    .error_text {
        color: red;
        font-size: 10px;
        text-align: center;
        margin: 0;
    }
    .star_required {
        color: red;
    }
    .required_description {
        padding: 6px 0 0 10px;
        font-size: 12px;
    }
</style>
<div class="callback_form">
    <? if (!$arResult["SUCCESS"]): ?>
        <p class="callback_header"><b>Оставьте ваши контакты и мы вам перезвоним</b></p>
    <? endif; ?>
    <? if ($arResult["SUCCESS"]): ?>
        <p class="callback_add_success"><?= $arResult["SUCCESS"] ?></p>
    <? endif; ?>
    <? if (!empty($arResult["FIELDS"])): ?>
        <form name="iblock_add" method="post">
            <? foreach ($arResult["FIELDS"] as $value): ?>
                <div class="callback_form_input<?= $arResult["ERROR_CLASS"][$value["CODE"]] ?>">
                    <?= $value["NAME"] ?><span class="star_required">*</span><br />
                    <? if ($arResult["ERROR_CLASS"][$value["CODE"]]): ?><p class="error_text"><?= GetMessage("CALLBACK_INPUT_REQUIRED") ?></p><? endif; ?>
                    <input type="text" id="prop_<?= $value["CODE"] ?>" name="<?= $value["CODE"] ?>" size="30" maxlength="60" value="<?= $arResult["FIELDS_VALUE"][$value["CODE"]] ?>" />
                </div>
            <? endforeach; ?>
            <div class="callback_form_submit">
                <input type="submit" name="callback_form_add" class="callback_form_add" value="<?= GetMessage("CALLBACK_FORM_ADD") ?>" />
            </div>
            <div class="required_description"><span class="star_required">*</span><?= GetMessage("CALLBACK_INPUT_REQUIRED") ?></div>
        </form>
    <? endif; ?>
</div>