<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

//Module
CModule::IncludeModule("iblock");

//arParams
$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

//Filter
$arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];

//arResult
$arResult = array();

//Error
$arResult["ERROR_CLASS"] = array();

if ($this->StartResultCache()) {
    $sql = CIBlock::GetProperties($arFilter["IBLOCK_ID"], Array(), Array());
    while ($res = $sql->GetNext()) {
        $arResult["FIELDS"][] = $res;
    }
}

if ($_POST["callback_form_add"]) {
    $quotes = array("!", "@", "#", "$", "%", "^", "’", "&", "*", "(", ")", "»", "№", ";", ":", "?", "”", "<", ">");
    foreach ($_POST as $key => $value) {
        $field = trim($value);
        if (empty($field) || strlen($field) == 1) {
            $arResult["ERROR_CLASS"][$key] = " error";
        } else {
            $field = str_replace( $quotes, '', $field );
            if ($key == "NAME" && preg_match('/[а-яА-Яa-zA-Z]/', $field) == false) {
                $arResult["ERROR_CLASS"][$key] = " error";
            } else {
                $arResult["FIELDS_VALUE"][$key] = htmlspecialchars($value);
            }
        }
    }
    if (empty($arResult["ERROR_CLASS"])) {
        $PROP = array();
        foreach ($arResult["FIELDS"] as $value) {
            $PROP[$value["ID"]] = $arResult["FIELDS_VALUE"][$value["CODE"]];
        }
        $arElement = array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "DATE_ACTIVE_FROM" => ConvertTimeStamp(mktime(0, 0, 0, 10, 25, 2003), "FULL", "ru"),
            "PROPERTY_VALUES" => $PROP,
            "NAME" => GetMessage("ELEMENT_NAME") . $arResult["FIELDS_VALUE"]["NAME"],
            "ACTIVE" => "Y"
        );
        $el = new CIBlockElement;
        if ($id = $el->Add($arElement)) {
            foreach ($arResult["FIELDS"] as $value) {
                $arEventFields[$value["CODE"]] = $arResult["FIELDS_VALUE"][$value["CODE"]];
            }
            CEvent::Send("CALLBACK", "s1", $arEventFields);
            $arResult["FIELDS"] = array();
            $arResult["SUCCESS"] = GetMessage("SUCCESS");
        } else {
            $arResult["ERROR"] = GetMessage("ERROR");
        }
    }
}

$this->IncludeComponentTemplate();
?>