<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

ksort($arResult['PRICE_FILTER']);
$arProperty['BASE'] = array('VALUE' => $arResult['PRICE_FILTER']);

$arProperty['PHENSHUI'] = array();
$rs = GetValueProp('PHENSHUI');
while($ar = $rs->Fetch()){
	$arProperty['PHENSHUI']['VALUE'][] = $ar['VALUE'];
}
sort($arProperty['PHENSHUI']['VALUE']);

$rs = GetValueProp('TYPE');
while($ar = $rs->Fetch()){
	$arProps[$ar['IBLOCK_PROPERTY_ID']][$ar['VALUE']] = array();
}

$rsEnum = CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID' => $arParams['IBLOCK_ID']));

while($ar = $rsEnum->Fetch()){
	if(is_array($arProps[$ar['PROPERTY_ID']][$ar['ID']]))
		$arProps[$ar['PROPERTY_ID']][$ar['ID']] = $ar;
}

$rsProp = CIBlockProperty::GetList(array(), array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y'));
$arException = array();
while($arProp = $rsProp->Fetch()){
	if(in_array($arProp['CODE'], $arException)) continue;
	
	$arProperty[$arProp['CODE']] = array_merge($arProp,$arProperty[$arProp['CODE']]);
	if(array_key_exists($arProp['ID'],$arProps)){
		$arProperty[$arProp['CODE']]['VALUE'] = $arProps[$arProp['ID']];
	}
}
$arResult['PROPERTIES'] = $arProperty;