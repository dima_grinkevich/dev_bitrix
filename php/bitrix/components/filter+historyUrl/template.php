<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div id="number_list">
	<table class="numbers">
		<thead>
		<tr>
			<?if(isset($arParams['AVAILABLE_SORT'])):?>
				
				<?$i=0; foreach($arParams['AVAILABLE_SORT'] as $key => $val):
					$className = ($arParams['ELEMENT_SORT_FIELD'] == $val[0]) ? ' selected ' : '';
					if ($className)
						$classOrder = ($arParams['ELEMENT_SORT_ORDER'] == 'asc') ? 'asc' : 'desc';
					$newSort = ($arParams['ELEMENT_SORT_ORDER'] == $val[1] && $className) ? ($arParams['ELEMENT_SORT_ORDER'] == 'desc' ? 'asc' : 'desc') : $arParams['AVAILABLE_SORT'][$key][1];
				?>
					<th <?if($i == 0):?>colspan="2"<?elseif($i > 2):?>class="center"<?endif;?>>
						<span class="wrapper-hover-menu <?=$key?>">
							<a href="<?=$APPLICATION->GetCurPageParam('sort='.$key.'&order='.$newSort, 	array('sort', 'order'))?>#number_list" class="numbers__sort link<?=$className?>"><?=GetMessage('TH_TITLE_'.$key)?></a>
							<?if($classOrder):?>
								<img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-sort <?=$classOrder?>">
							<?endif;?>
							<?if($key == 'name'):?>
								<span class="hover-menu">
									<input type="text" name="<?=$arParams['FILTER_NAME']?>_PROPERTY_PHONE_NUMBER" value=""/>
									<a href="#" class="search-link"></a>
								</span>
							<?elseif($key == 'type'):?>
								<span class="hover-menu">
									<label>
										<input type="radio" name="<?=$arParams['FILTER_NAME']?>_PROPERTY_TYPE" value="all" data-count="optional"/>
										Все типы
									</label>
									<?foreach($arResult['PROPERTIES']['TYPE']['VALUE'] as $arValue):?>
										<label>
											<input type="radio" name="<?=$arParams['FILTER_NAME']?>_PROPERTY_TYPE" value="<?=$arValue['ID']?>" data-count="optional"/>
											<?=$arValue['VALUE']?>
										</label>
									<?endforeach;?>
								</span> <!-- end hover-menu" -->
							<?elseif($key == 'phenshui'):?>
								<span class="hover-menu">
									<label>
										<input type="checkbox" name="<?=$arParams['FILTER_NAME']?>_PROPERTY_PHENSHUI[]" value="all" data-count="optional"/>
										Все
									</label>
									<?foreach($arResult['PROPERTIES']['PHENSHUI']['VALUE'] as $v):?>
										<label>
											<input type="checkbox" name="<?=$arParams['FILTER_NAME']?>_PROPERTY_PHENSHUI[]" value="<?=$v?>" data-count="optional"/>
											<?=$v?>
										</label>
									<?endforeach;?>
								</span> <!-- end hover-menu" -->
							<?elseif($key == 'price'):?>
								<span class="hover-menu">
									<label>
										<input type="checkbox" name="<?=$arParams['FILTER_NAME']?>_CATALOG_PRICE_1[]" value="all" data-count="optional"/>
										Все
									</label>
									<?foreach($arResult['PROPERTIES']['BASE']['VALUE'] as $k => $val):?>
										<label>
											<input type="checkbox" name="<?=$arParams['FILTER_NAME']?>_CATALOG_PRICE_1[]" value="<?=$k?>" data-count="optional"/>
											<?=$val?>
										</label>
									<?endforeach;?>
								</span> <!-- end hover-menu" -->
							<?endif;?>
						</span>
					</th>
				<?
				unset($className);
				unset($classOrder);
				$i++;
				endforeach;?>
			<?else: //если не переданы сортировки?>
				<th colspan="2">
					<span class="wrapper-hover-menu name">
						<a href="#" class="numbers__sort link">Поиск номера</a>
						<img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-sort">
						<span class="hover-menu">
							<input type="text" name="NAME"/>
							<a href="#" class="search-link"></a>
						</span>
					</span>
				</th>
				<th>
					<span class="wrapper-hover-menu">
						<a href="#" class="numbers__sort link">Тип номера</a>
						<img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-sort">
						<span class="hover-menu">
							<label>
								<input type="checkbox" name="PROPERTY_TYPE" value="" data-count="optional"/>
								Все типы
							</label>
							<?foreach($arResult['PROPERTIES']['TYPE']['VALUE'] as $arValue):?>
								<label>
									<input type="checkbox" name="PROPERTY_TYPE" value="<?=$arValue['ID']?>" data-count="optional"/>
									<?=$arValue['VALUE']?>
								</label>
							<?endforeach;?>
						</span> <!-- end hover-menu" -->
					</span> <!-- end wrapper-hover-menu -->
				</th>
				<th class="center">
					<a href="#" class="numbers__sort link">Феншуй</a>
					<img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-sort">
				</th>
				<th class="center">
					<a href="#" class="numbers__sort link">Цена</a>
					<img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-sort">
				</th>
			<?endif;?>
			<th></th>
		</tr>
		</thead>
		<tbody>
		<? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
		<tr class="numbers__item<?/*numbers__item_in_cart*/?>">
			<td class="numbers__code">
				<div class="numbers__column">
				<?
					$section = getIblockSections(array("ID" => $arItem["IBLOCK_SECTION_ID"]));
					$section = $section[0];
				?>
				<?//var_dump($section);?>
					+7 (<?=$section["NAME"]?>)
				</div>
			</td>
			<td class="numbers__num">
				<div class="numbers__column">
					<?=$arItem["NAME"]?>
				</div>
			</td>
			<td class="numbers__type">
				<div class="numbers__column">
				<?if(strlen($arItem['PROPERTIES']['TYPE']['VALUE']) > 0):?>
					<img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-<?=$arItem['PROPERTIES']['TYPE']['VALUE_XML_ID']?>">
					<span class="middle"><?=$arItem['PROPERTIES']['TYPE']['VALUE']?></span>
				<?else:?>
					&nbsp;
				<?endif;?></div>
			</td>
			<td class="numbers__shui">
				<div class="numbers__column">
					<?=$arItem['PROPERTIES']['PHENSHUI']['VALUE'] ? : '&nbsp;'?>
				</div>
			</td>
			<td class="numbers__price">
				<div class="numbers__column">
					<?=$arItem['PRICES']['BASE']['PRINT_VALUE'] ? : CurrencyFormat(0, 'RUB')?>
				</div>
			</td>
			<td class="numbers__button">
				<a href="#" class="numbers__buy buy-btn-click" data-id="<?=$arItem["ID"]?>">
					<img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-cart">
				</a>
			</td>
		</tr>
		<? endforeach; ?>


		</tbody>
	</table>

	<?if(intval($arResult['NAV_RESULT']->NavPageNomer) !== intval($arResult['NAV_RESULT']->NavPageCount)):?>
		<div class="more center" id="ajax_load">
		<a href="#" class="link link_ajax" onclick="return ajaxPagination('<?=$APPLICATION->GetCurPageParam();?>', 'PAGEN_<?=trim($arResult['NAV_RESULT']->SIZEN)?>', '<?=$arResult['NAV_RESULT']->NavPageNomer?>');">
			<img src="<?=SITE_TEMPLATE_PATH?>/images/px-2daeaa8b.gif" class="ico ico-refresh">
			<span>Еще номера</span></a>
		</div>
	<?endif;?>
</div>










<?
/*
 * $arItem['SECOND_PICT']
 *//*
if (!empty($arResult['ITEMS']))
{
	CJSCore::Init(array("popup"));
	$arSkuTemplate = array();
	if (!empty($arResult['SKU_PROPS']))
	{
		foreach ($arResult['SKU_PROPS'] as &$arProp)
		{
			ob_start();
			if ('L' == $arProp['PROPERTY_TYPE'])
			{
				if (5 < $arProp['VALUES_COUNT'])
				{
					$strClass = 'bx_item_detail_size full';
					$strWidth = ($arProp['VALUES_COUNT']*20).'%';
					$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
					$strSlideStyle = '';
				}
				else
				{
					$strClass = 'bx_item_detail_size';
					$strWidth = '100%';
					$strOneWidth = '20%';
					$strSlideStyle = 'display: none;';
				}
				?><div class="<? echo $strClass; ?>" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_cont">
<span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>
<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_<? echo $arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;"><?
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					?><li
						data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID']; ?>"
						data-onevalue="<? echo $arOneValue['ID']; ?>"
						style="width: <? echo $strOneWidth; ?>;"
					><span></span><a href="javascript:void(0)"><? echo htmlspecialcharsex($arOneValue['NAME']); ?></a></li><?
				}
?></ul></div>
		<div class="bx_slide_left" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
		<div class="bx_slide_right" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
	</div>
</div><?
			}
			elseif ('E' == $arProp['PROPERTY_TYPE'])
			{
				if (5 < $arProp['VALUES_COUNT'])
				{
					$strClass = 'bx_item_detail_scu full';
					$strWidth = ($arProp['VALUES_COUNT']*20).'%';
					$strOneWidth = (100/$arProp['VALUES_COUNT']).'%';
					$strSlideStyle = '';
				}
				else
				{
					$strClass = 'bx_item_detail_scu';
					$strWidth = '100%';
					$strOneWidth = '20%';
					$strSlideStyle = 'display: none;';
				}
				?><div class="<? echo $strClass; ?>" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_cont">
<span class="bx_item_section_name_gray"><? echo htmlspecialcharsex($arProp['NAME']); ?></span>
<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_<? echo $arProp['ID']; ?>_list" style="width: <? echo $strWidth; ?>;"><?
				foreach ($arProp['VALUES'] as $arOneValue)
				{
					?><li
						data-treevalue="<? echo $arProp['ID'].'_'.$arOneValue['ID'] ?>"
						data-onevalue="<? echo $arOneValue['ID']; ?>"
						style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>;"
						><span></span><a href="javascript:void(0)"><span style="background-image:url('<? echo $arOneValue['PICT']['SRC']; ?>');"></span></a></li><?
				}
?></ul></div>
		<div class="bx_slide_left" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_left" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
		<div class="bx_slide_right" id="#ITEM#_prop_<? echo $arProp['ID']; ?>_right" data-treevalue="<? echo $arProp['ID']; ?>" style="<? echo $strSlideStyle; ?>"></div>
	</div>
</div><?
			}
			$arSkuTemplate[$arProp['CODE']] = ob_get_contents();
			ob_end_clean();
		}
		unset($arProp);
	}
	if ($arParams["DISPLAY_TOP_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}

	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?><div class="bx_catalog_list_home col<? echo $arParams['LINE_ELEMENT_COUNT']; ?>"><?
foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);

	$arItemIDs = array(
		'ID' => $strMainID,
		'PICT' => $strMainID.'_pict',
		'SECOND_PICT' => $strMainID.'_secondpict',
		'MAIN_PROPS' => $strMainID.'_main_props',

		'QUANTITY' => $strMainID.'_quantity',
		'QUANTITY_DOWN' => $strMainID.'_quant_down',
		'QUANTITY_UP' => $strMainID.'_quant_up',
		'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
		'BUY_LINK' => $strMainID.'_buy_link',
		'SUBSCRIBE_LINK' => $strMainID.'_subscribe',

		'PRICE' => $strMainID.'_price',
		'DSC_PERC' => $strMainID.'_dsc_perc',
		'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',

		'PROP_DIV' => $strMainID.'_sku_tree',
		'PROP' => $strMainID.'_prop_',
		'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop'

	);

	$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/i", "x", $strMainID);

	?><div class="<? echo ($arItem['SECOND_PICT'] ? 'bx_catalog_item double' : 'bx_catalog_item'); ?>"><div class="bx_catalog_item_container" id="<? echo $strMainID; ?>">
		<a id="<? echo $arItemIDs['PICT']; ?>"
			href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"
			class="bx_catalog_item_images"
			style="background-image: url(<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>)" title=""><?
	if ('Y' == $arParams['SHOW_DISCOUNT_PERCENT'])
	{
	?>
			<div
				id="<? echo $arItemIDs['DSC_PERC']; ?>"
				class="bx_stick_disc right bottom"
				style="display:<? echo (0 < $arItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] ? '' : 'none'); ?>;">-<? echo $arItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%</div>
	<?
	}
	if ($arItem['LABEL'])
	{
	?>
			<div class="bx_stick average left top" title="<? echo $arItem['LABEL_VALUE']; ?>"><? echo $arItem['LABEL_VALUE']; ?></div>
	<?
	}
	?>
		</a><?
	if ($arItem['SECOND_PICT'])
	{
		?><a id="<? echo $arItemIDs['SECOND_PICT']; ?>"
			href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"
			class="bx_catalog_item_images_double"
			style="background-image: url(<? echo (
				!empty($arItem['PREVIEW_PICTURE_SECOND'])
				? $arItem['PREVIEW_PICTURE_SECOND']['SRC']
				: $arItem['PREVIEW_PICTURE']['SRC']
			); ?>)"  title=""><?
		if ('Y' == $arParams['SHOW_DISCOUNT_PERCENT'])
		{
		?>
			<div
				id="<? echo $arItemIDs['SECOND_DSC_PERC']; ?>"
				class="bx_stick_disc right bottom"
				style="display:<? echo (0 < $arItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] ? '' : 'none'); ?>;">-<? echo $arItem['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']; ?>%</div>
		<?
		}
		if ($arItem['LABEL'])
		{
		?>
			<div class="bx_stick average left top" title="<? echo $arItem['LABEL_VALUE']; ?>"><? echo $arItem['LABEL_VALUE']; ?></div>
		<?
		}
		?>
		</a><?
	}
	?><div class="bx_catalog_item_title"><a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" title="<? echo $arItem['NAME']; ?>"><? echo $arItem['NAME']; ?></a></div>
	<div class="bx_catalog_item_price"><div id="<? echo $arItemIDs['PRICE']; ?>" class="bx_price"><?
	if (!empty($arItem['MIN_PRICE']))
	{
		if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
			echo GetMessage('CT_BCS_TPL_MESS_PRICE_FROM').' ';
		echo $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
		if ('Y' == $arParams['SHOW_OLD_PRICE'] && $arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE'])
		{
			?> <span><? echo $arItem['MIN_PRICE']['PRINT_VALUE']; ?></span><?
		}
	}
	?></div></div><?
	if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS']))
	{
		?><div class="bx_catalog_item_controls"><?
		if ($arItem['CAN_BUY'])
		{
			if ('Y' == $arParams['USE_PRODUCT_QUANTITY'])
			{
			?>
		<div class="bx_catalog_item_controls_blockone"><div style="display: inline-block;position: relative;">
			<a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>" href="javascript:void(0)" class="bx_bt_white bx_small" rel="nofollow">-</a>
			<input type="text" class="bx_col_input" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
			<a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0)" class="bx_bt_white bx_small" rel="nofollow">+</a>
			<span id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"><? echo $arItem['CATALOG_MEASURE_NAME']; ?></span>
		</div></div>
			<?
			}
			?>
		<div class="bx_catalog_item_controls_blocktwo">
			<a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="bx_bt_blue bx_medium" href="javascript:void(0)" rel="nofollow"><?
			echo ('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCS_TPL_MESS_BTN_BUY'));
			?></a>
		</div>
			<?
		}
		else
		{
			?><div class="bx_catalog_item_controls_blockone"><span class="bx_notavailable"><?
			echo ('' != $arParams['MESS_NOT_AVAILABLE'] ? $arParams['MESS_NOT_AVAILABLE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE'));
			?></span></div><?
			if ('Y' == $arParams['PRODUCT_SUBSCRIPTION'] && 'Y' == $arItem['CATALOG_SUBSCRIPTION'])
			{
			?>
			<div class="bx_catalog_item_controls_blocktwo">
				<a
					id="<? echo $arItemIDs['SUBSCRIBE_LINK']; ?>"
					class="bx_bt_white bx_medium"
					href="javascript:void(0)"><?
					echo ('' != $arParams['MESS_BTN_SUBSCRIBE'] ? $arParams['MESS_BTN_SUBSCRIBE'] : GetMessage('CT_BCS_TPL_MESS_BTN_SUBSCRIBE'));
					?>
				</a>
			</div><?
			}
		}
		?><div style="clear: both;"></div></div><?
		if (isset($arItem['DISPLAY_PROPERTIES']) && !empty($arItem['DISPLAY_PROPERTIES']))
		{
?>
			<div class="bx_catalog_item_articul">
<?
			foreach ($arItem['DISPLAY_PROPERTIES'] as $arOneProp)
			{
				?><br><strong><? echo $arOneProp['NAME']; ?></strong> <?
					echo (
						is_array($arOneProp['DISPLAY_VALUE'])
						? implode('<br>', $arOneProp['DISPLAY_VALUE'])
						: $arOneProp['DISPLAY_VALUE']
					);
			}
?>
			</div>
<?
		}
		$arJSParams = array(
			'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_ADD_BASKET_BTN' => false,
			'SHOW_BUY_BTN' => true,
			'SHOW_ABSENT' => true,
			'PRODUCT' => array(
				'ID' => $arItem['ID'],
				'NAME' => $arItem['~NAME'],
				'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
				'CAN_BUY' => $arItem["CAN_BUY"],
				'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
				'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
				'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
				'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
				'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
				'ADD_URL' => $arItem['~ADD_URL'],
				'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL']
			),
			'VISUAL' => array(
				'ID' => $arItemIDs['ID'],
				'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
				'QUANTITY_ID' => $arItemIDs['QUANTITY'],
				'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
				'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
				'PRICE_ID' => $arItemIDs['PRICE'],
				'BUY_ID' => $arItemIDs['BUY_LINK'],
			),
			'AJAX_PATH' => POST_FORM_ACTION_URI
		);
		?><script type="text/javascript">
		var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
		</script><?
	}
	else
	{
		if ('Y' == $arParams['PRODUCT_DISPLAY_MODE'])
		{
			?>
		<div class="bx_catalog_item_controls no_touch">
			<?
			if ('Y' == $arParams['USE_PRODUCT_QUANTITY'])
			{
			?>
		<div class="bx_catalog_item_controls_blockone">
			<a id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>" href="javascript:void(0)" class="bx_bt_white bx_small" rel="nofollow">-</a>
			<input type="text" class="bx_col_input" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<? echo $arItem['CATALOG_MEASURE_RATIO']; ?>">
			<a id="<? echo $arItemIDs['QUANTITY_UP']; ?>" href="javascript:void(0)" class="bx_bt_white bx_small" rel="nofollow">+</a>
			<span id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"></span>
		</div>
			<?
			}
			?>
		<div class="bx_catalog_item_controls_blocktwo">
			<a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="bx_bt_blue bx_medium" href="javascript:void(0)" rel="nofollow"><?
			echo ('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCS_TPL_MESS_BTN_BUY'));
			?></a>
		</div>
				<div style="clear: both;"></div>
			</div>
			<div class="bx_catalog_item_controls touch">
				<a class="bx_bt_white bx_medium" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><?
				echo ('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL'));
				?></a>
			</div>
			<?
		}
		else
		{
			?>
		<div class="bx_catalog_item_controls no_touch">
			<a class="bx_bt_white bx_medium" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><?
			echo ('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL'));
			?></a>
		</div>
			<?
		}
		?>
		<div class="bx_catalog_item_controls touch">
			<a class="bx_bt_white bx_medium" href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><?
			echo ('' != $arParams['MESS_BTN_DETAIL'] ? $arParams['MESS_BTN_DETAIL'] : GetMessage('CT_BCS_TPL_MESS_BTN_DETAIL'));
			?></a>
		</div>
		<?
		$boolShowOfferProps = ('Y' == $arParams['PRODUCT_DISPLAY_MODE'] && $arItem['OFFERS_PROPS_DISPLAY']);
		$boolShowProductProps = (isset($arItem['DISPLAY_PROPERTIES']) && !empty($arItem['DISPLAY_PROPERTIES']));
		if ($boolShowProductProps || $boolShowOfferProps)
		{
?>
			<div class="bx_catalog_item_articul">
<?
			if ($boolShowProductProps)
			{
				foreach ($arItem['DISPLAY_PROPERTIES'] as $arOneProp)
				{
				?><br><strong><? echo $arOneProp['NAME']; ?></strong> <?
					echo (
						is_array($arOneProp['DISPLAY_VALUE'])
						? implode(' / ', $arOneProp['DISPLAY_VALUE'])
						: $arOneProp['DISPLAY_VALUE']
					);
				}
			}
			if ($boolShowOfferProps)
			{
?>
				<span id="<? echo $arItemIDs['DISPLAY_PROP_DIV']; ?>" style="display: none;"></span>
<?
			}
?>
			</div>
<?
		}
		if ('Y' == $arParams['PRODUCT_DISPLAY_MODE'])
		{
			if (!empty($arItem['OFFERS_PROP']))
			{
				$arSkuProps = array();
				?><div class="bx_catalog_item_scu" id="<? echo $arItemIDs['PROP_DIV']; ?>"><?
				foreach ($arSkuTemplate as $code => $strTemplate)
				{
					if (!isset($arItem['OFFERS_PROP'][$code]))
						continue;
					echo '<div>', str_replace('#ITEM#_prop_', $arItemIDs['PROP'], $strTemplate), '</div>';
				}
				foreach ($arResult['SKU_PROPS'] as $arOneProp)
				{
					if (!isset($arItem['OFFERS_PROP'][$arOneProp['CODE']]))
						continue;
					$arSkuProps[] = array(
						'ID' => $arOneProp['ID'],
						'TYPE' => $arOneProp['PROPERTY_TYPE'],
						'VALUES_COUNT' => $arOneProp['VALUES_COUNT']
					);
				}
				foreach ($arItem['JS_OFFERS'] as &$arOneJs)
				{
					if (0 < $arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'])
						$arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'] = '-'.$arOneJs['PRICE']['DISCOUNT_DIFF_PERCENT'].'%';
				}
				unset($arOneJs);
				?></div><?
				if ($arItem['OFFERS_PROPS_DISPLAY'])
				{
					foreach ($arItem['JS_OFFERS'] as $keyOffer => $arJSOffer)
					{
						$strProps = '';
						if (!empty($arJSOffer['DISPLAY_PROPERTIES']))
						{
							foreach ($arJSOffer['DISPLAY_PROPERTIES'] as $arOneProp)
							{
								$strProps .= '<br>'.$arOneProp['NAME'].' <strong>'.(
									is_array($arOneProp['VALUE'])
									? implode(' / ', $arOneProp['VALUE'])
									: $arOneProp['VALUE']
								).'</strong>';
							}
						}
						$arItem['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
					}
				}
				$arJSParams = array(
					'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
					'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
					'SHOW_ADD_BASKET_BTN' => false,
					'SHOW_BUY_BTN' => true,
					'SHOW_ABSENT' => true,
					'SHOW_SKU_PROPS' => $arItem['OFFERS_PROPS_DISPLAY'],
					'SECOND_PICT' => $arItem['SECOND_PICT'],
					'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
					'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
					'DEFAULT_PICTURE' => array(
						'PICTURE' => $arItem['PREVIEW_PICTURE'],
						'PICTURE_SECOND' => $arItem['PREVIEW_PICTURE_SECOND']
					),
					'VISUAL' => array(
						'ID' => $arItemIDs['ID'],
						'PICT_ID' => $arItemIDs['PICT'],
						'SECOND_PICT_ID' => $arItemIDs['SECOND_PICT'],
						'QUANTITY_ID' => $arItemIDs['QUANTITY'],
						'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
						'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
						'QUANTITY_MEASURE' => $arItemIDs['QUANTITY_MEASURE'],
						'PRICE_ID' => $arItemIDs['PRICE'],
						'TREE_ID' => $arItemIDs['PROP_DIV'],
						'TREE_ITEM_ID' => $arItemIDs['PROP'],
						'BUY_ID' => $arItemIDs['BUY_LINK'],
						'ADD_BASKET_ID' => $arItemIDs['ADD_BASKET_ID'],
						'DSC_PERC' => $arItemIDs['DSC_PERC'],
						'SECOND_DSC_PERC' => $arItemIDs['SECOND_DSC_PERC'],
						'DISPLAY_PROP_DIV' => $arItemIDs['DISPLAY_PROP_DIV'],
					),
					'OFFERS' => $arItem['JS_OFFERS'],
					'OFFER_SELECTED' => $arItem['OFFERS_SELECTED'],
					'TREE_PROPS' => $arSkuProps,
					'AJAX_PATH' => POST_FORM_ACTION_URI
				);
				?>
<script type="text/javascript">
var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
</script>
				<?
			}
		}
	}
?></div></div><?
}
?><div style="clear: both;"></div>
</div>
<script type="text/javascript">
BX.message({
	setButtonBuyName: '<? echo GetMessageJS("CATALOG_SET_BUTTON_BUY"); ?>',
	setButtonBuyUrl: '<? echo $arParams["BASKET_URL"]; ?>',
	ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>'
});
</script>
<?
	if ($arParams["DISPLAY_BOTTOM_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
}*/
?>