<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script>
	var oPrices = <?=CUtil::PhpToJSObject($arResult['arPrices'])?>;
	var oOffers = <?=CUtil::PhpToJSObject($arResult['arOffers'])?>;
	var oOffers2 = <?=CUtil::PhpToJSObject($arResult['OFFERS_2'])?>;
	var oProps = <?=CUtil::PhpToJSObject($arResult['PROPS'])?>;
	var color_tovar = <?=CUtil::PhpToJSObject($arResult['COLORS'])?>; 
	var color_inv = <?=CUtil::PhpToJSObject($arResult['OFFER_COLORS_INV'])?>; 
	var complex = <?=CUtil::PhpToJSObject($arResult['arOffers_COMPLEX'])?>; 
	//var offers = <?=CUtil::PhpToJSObject($arResult['arPrices'])?>;
</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody><tr>
<td valign="top" align="left" class="cd1">

</td>
<td valign="top" align="left" width="99%">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td valign="top" class="cd1"><table width="100%" border="0" cellpadding="5" cellspacing="0">
        <tbody><tr>
          <td valign="top" class="cd2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr valign="top">
                <td width="1%" class="cd3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                      <td><img alt="" src="<?=SITE_DIR?>images/dot.gif" border="0" width="300" height="1"></td>
                    </tr>
                    <tr>
                      <td>
					  <?if(is_array($arResult['PICTURE'])):?>
							<a class="thickbox" href="<?=$arResult['PICTURE']['BIG']['SRC']?>"><img alt="" title="<?=$arResult['NAME']?>" vspace="0" hspace="0" border="0" src="<?=$arResult['PICTURE']['SMALL']['SRC']?>"></a><span class="small"><br><?=GetMessage('CLICK_FOR_ZOOM')?></span>
					  <?else:?>
						<div class="no-pic">
					  <?endif?>
					  </td>
                    </tr>
                  </tbody></table></td>
                <td width="99%" valign="top" align="right">
				<?/*/?><iframe src="./prosto_files/iframecart.htm" frameborder="0" style="border:0px; width:100%; height:240px"></iframe>
				
                <?//*/?>
				
				<?if($arResult['COMPLEX_ALGORITHM'] != 'Y'):?>
				<form name="addToCart" method="post" target="cart">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						
						<tbody><tr>
						<td valign="top" align="left">
						<?/*/?>
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tbody>
								<tr>
									<td valign="top" align="left" class="cl10">&nbsp;</td>
									<td valign="top" align="left">
									<?if(count($arResult['OFFERS_PROP']) > 0):?><a class="reset_select" style="display:none"><?=GetMessage('RESET')?></a><?endif;?></td>
								</tr>
								<?
								$arOfferFirst = $arResult['OFFERS'][0];
								foreach($arResult['OFFERS_PROP'] as $key => $arProp):
									$cnt = count($arProp['VALUES']);
								?>
								
								<tr <?= $cnt > 1 ? '' : 'class="_hide"'?>>
								<td valign="top" align="left" class="cl10"><?=$arProp['NAME']?>:&nbsp;</td>
								<td valign="top" align="left">
								
								
										<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tbody><tr>
											<td valign="top" align="left">
											
												<?if($cnt > 1):?>
													<select name="params[]" class="prop_price" data-code="<?=ToLower($key)?>">
													<?if($key !== 'COLOR'):
														foreach($arProp['VALUES'] as $val => $prop):?>
															<option value="<?=replaceSpace($val)?>" class="<?=setClass($prop)?>" <?= $val == $arOfferFirst['PROPERTIES'][$key]['VALUE'] ? 'selected data-def="Y"' : ''?>><?=$val?></option>
														<?endforeach;
													else:
														foreach($arProp['VALUES'] as $val => $prop):
															$arColor = $arResult['OFFER_COLORS'][$val];
														?>
															<option value="<?=$val?>" style="background-color:<?=$arColor['PROPERTY_NUMBER_COLOR_VALUE'];?>" class="0 <?=setClass($prop)?>" <?= $val == $arOfferFirst['PROPERTIES'][$key]['VALUE'] ? 'selected data-def="Y"' : ''?>><?= $arColor['NAME'] ? : '--'?></option>
														<?endforeach;
													endif;?>
													</select>
												<?elseif($cnt == 1 && $val != '--' && !empty($val)):?>
													<input type="text" disabled name="params[]" value="<?=$val?>">
												<?else:?>
													<input type="hidden" name="params[]" value="--">
												<?endif;?> 
											</td>
										</tr>
										</tbody></table>
								</td>
								</tr>	
								<?endforeach;?>
								
							<tr>
								<td valign="top" align="left" class="cl12">Цена за <span style="white-space:nowrap">м2</span>:</td>
								<td valign="top" align="left" class="cl13"><div id="cost" data-cost="<?=$arResult['arPrices'][$arOfferFirst['ID']]?>"><?=$arResult['arPrices'][$arOfferFirst['ID']]?></div>
								<input type="hidden" id="item" name="item" data-value="<?=$arOfferFirst['ID']?>" value="<?=$arOfferFirst['ID']?>"></td>
							</tr>

							
							
							
					</tbody></table>
					<?//*/?>
					
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tbody>
								<tr>
									<td valign="top" align="left" class="cl10">&nbsp;</td>
									<td valign="top" align="left">
									<?if(count($arResult['OFFERS_PROPS']) > 1):?><a class="reset_select" style="display:none"><?=GetMessage('RESET')?></a><?endif;?></td>
								</tr>
								<?
								$arOfferFirst = $arResult['OFFERS'][0];
								foreach($arResult['OFFERS_PROPS'] as $key => $arProp):
									$cnt = count($arProp['VALUES'])-1;
								?>
								
								<tr <?= $cnt > 1 ? '' : 'class="_hide"'?>>
								<td valign="top" align="left" class="cl10"><?=$arProp['NAME']?>:&nbsp;</td>
								<td valign="top" align="left">
								
								
										<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tbody><tr>
											<td valign="top" align="left">
											
												<?if($cnt > 1):?>
													<select name="params[]" class="prop_price" data-code="<?=$key?>">
													<?if(isset($arProp['VALUES']['--'])):?>
														<option value="--" class="<?=implode(' ', $arProp['VALUES']['--']['VALUES'])?>" ><?=$arProp['VALUES']['--']['NAME']?></option>
													<?unset($arProp['VALUES']['--']);
													endif;?>
													<?foreach($arProp['VALUES'] as $val => $prop):?>
														<option value="<?=replaceSpace($val)?>" class="<?=implode(' ', $prop['VALUES'])?>" ><?=$prop['NAME']?></option>
													<?endforeach;?>
													</select>
												<?elseif($cnt == 1 && $val != '--' && !empty($val)):?>
													<input type="text" disabled name="params[]" value="<?=$val?>">
												<?else:?>
													<input type="hidden" name="params[]" value="--">
												<?endif;?> 
											</td>
										</tr>
										</tbody></table>
								</td>
								</tr>	
								<?endforeach;?>
								
							<tr>
								<td valign="top" align="left" class="cl12">Цена за <span style="white-space:nowrap">м2</span>:</td>
								<td valign="top" align="left" class="cl13"><div id="cost" data-cost="<?=$arResult['arPrices'][$arOfferFirst['ID']]?>"><?=$arResult['arPrices'][$arOfferFirst['ID']]?></div>
								<input type="hidden" id="item" name="item" data-value="<?=$arOfferFirst['ID']?>" value="<?=$arOfferFirst['ID']?>"></td>
							</tr>

							
							
							
					</tbody></table>
						</td>
						</tr>
						
						<tr>
							<td><b>Заказать:</b></td>
						</tr>
						<tr>
							<td width="1%">
							
							<script type="text/javascript">
							$(document).ready(function(){jQuery(".qty486").bind('keyup keypress change', function(event){checkNumberFields(this, event);})});
							</script>
							
								<table width="1%" border="0" cellspacing="0" cellpadding="0">
									<tbody><tr>
										<td height="40"><input name="qty" type="text" class="qty486" value="1" size="5" style="width: 60px"></td>
										<td width="1%" nowrap="">&nbsp;м2&nbsp;</td>
										<td width="1%">
										<input style="border:0px; background:url(/images/buy.gif); width:83px; height:20px; cursor:pointer" type="button" class=""
										data-href="<?=$APPLICATION->GetCurPage()?>" data-cart="" title="Добавить в корзину" onclick="addToCart2();"></td>
									</tr>
								</tbody></table>
							</td>
						</tr>
					</tbody></table>
					</form>

<?//*/?>
				<?else:?>
												
					<table width="250" border="0" cellpadding="0" cellspacing="0" class="ordertable">
					<tbody><tr>
					<td>


					<a href="#obsh_block" class="fancybox"><input style="margin-top: 40px; margin-left: 20px; border-radius: 8px; text-decoration: none; color: white;" id="view_blocki" class="button_ob order" type="button" value="Купить" onclick="return false;"></a>


					</td>
					</tr>
					</tbody></table>


					<br>
					<img src="/images/dot.gif" alt="" width="300" height="1" border="0">
					
					
<div id="obsh_block" style="display: none;">
<div class="calcrelative">
<div class="menuviborkatop">
<div id="ident1" style="color: black; text-shadow: none;">Выбрать материал</div>
<div id="ident2" style="color: rgb(255, 255, 224); text-shadow: none;">Задать размеры и цвет</div>
<div id="ident3" style="color: rgb(255, 255, 224); text-shadow: none;">Заказать</div>
</div>
<div class="viborka1" style="display: block;">

	<div class="calcnotice">Выберите тип и толщину покрытия. На пересечении типа и толщины показана цена за погонный/квадратный метр в рублях.</div><br><br>
<div class="calcnotice" style="float: right;">Толщина указана в мм.</div>
<span style="font-weight: bold; color: red;" id="error_pole2"></span>	
<table style="width: 100%;">
	<tbody><tr>
<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
<td style="text-align: right;"></td>	
</tr>
</tbody></table>
<table style="width: 100%;">
<tbody><tr title="Толщина"><td id="block">Покрытие</td>
<td title="Еденица измерения" id="block">Ед. изм</td>
<?foreach($arResult['THICKNESS_SORT'] as $val):?>
<td id="block"><?=$val?></td> 
<?endforeach;?>
</tr>
<?
$sKoeff = $arResult['PROPERTIES']['COEFFICIENT']['VALUE'] ? : 1;?><?

foreach($arResult['arOffers_COMPLEX'] as $k1 => $arValues):?>
<tr>
<td><?=$k1?></td>
<td>м.п.</td>
	<?foreach($arResult['THICKNESS_SORT'] as $sort => $val):?>
		<?if($arValues[$val]):?>
			<td>
			<?$arColor = $arOffer = $arPrice = array();
			foreach($arValues[$val] as $k3 => $value):
				$arPrice[] = str_replace(array(' ', 'руб.'), '', $arResult['arPrices'][$value]);
				$arColor[] = $arResult['OFFER_COLORS'][$k3]['NAME'];
				$arOffer[] = $value;
				//$articul = $arResult['OFFERS'][$arResult['KEYS'][$value]]['PROPERTIES']['ARTICUL']['VALUE'];
			endforeach;?>
			<input id="width" type="radio" name="width" value="<?=$k1?>*<?=$val?>*<?=implode('_',$arPrice)?>*<?=implode('_',$arColor)?>*0*0*0*Погонные метры*<?=implode('_',$arOffer)?>*<?=$sKoeff?>*"><br>
			</td>
		<?else:?>
			<td>&nbsp;</td>
		
		<?endif;?>
	<?endforeach;?>
</tr>


<?endforeach;?>


</tbody></table>
<table style="width: 100%;">
<tbody><tr>
<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
<td style="text-align: right;"><a id="viborka_next1" href="<?=$APPLICATION->GetCurPage()?>" onclick="return false;"><div class="button" style="position: relative;">Далее<img src="/images//strel_button.png" style="position:absolute;  height: 100%; right: 0; top: 0;"></div></a><a id="bistr_zayavka" class="fancybox" href="#block_bistr_zayav" onclick="return false;"><div class="button">Быстрая заявка</div></a></td>	
</tr>
</tbody></table>
	
</div>	
<div class="viborka2" style="display: none;">
<p><span><?=$arResult['NAME']?></span> <span id="tovar_name">Оцинкованный</span> <span id="tovar_tolsh">0.6мм</span></p>
<p><span style="font-weight: bold; color: red;" id="error_pole"> </span></p>
<script type="text/javascript">
	


</script>
<div class="calcnotice">Укажите желаемый цвет, длину листа и количество листов. Вы можете заказать листы разной длины, воспользовавшись кнопкой "Добавить позицию". С помощью кнопки "Быстрая заявка" можно отправить заявку в произвольной форме менеджеру по продажам. </div>
<table style="width: 100%;" id="spis_table">
<tbody><tr>
<td></td><td></td><td style="text-align: right;"></td>
</tr>
<tr>
<td>Цвет</td><td>Длина листа, м</td><td>Количество листов, шт</td>	
</tr>
<tr style="color: black;" id="table_info"><td><select id="pole10"><option style="background-color: white;">Оцинкованный</option></select></td><td><input id="pole20" placeholder="1-12 м" type="text" size="8" maxlength="8" name="dlinna"></td><td><input id="pole30" type="text" size="8" maxlength="50" name="colichestvo"></td></tr>	



</tbody></table>

<table style="width: 100%;">	
<tbody><tr>
<td><a id="add_pole" href="<?=$APPLICATION->GetCurPage()?>" onclick="return false;">Добавить позицию</a><a id="delete_pole" href="<?=$APPLICATION->GetCurPage()?>" onclick="return false;">Удалить позицию</a></td>
<td></td>
<td style="text-align: right;"></td>	
</tr>
<tr>
<td style="text-align: left;"><a id="viborka_back2" href="<?=$APPLICATION->GetCurPage()?>" onclick="return false;"><div class="button" style="position: relative;">Назад<img src="/images/strel_button2.png" style="position:absolute;  height: 100%; left: 0; top: 0;"></div></a></td>
<td></td>
<td style="text-align: right;"><a id="viborka_next2" href="<?=$APPLICATION->GetCurPage()?>" onclick="return false;"><div style="position: relative;" class="button">Далее<img src="/images/strel_button.png" style="position:absolute;  height: 100%; right: 0; top: 0;"></div></a><a id="bistr_zayavka" class="fancybox" href="#block_bistr_zayav" onclick="return false;"><div class="button">Быстрая заявка</div></a></td>
</tr>	
<tr>
<td></td>
<td></td>
<td style="text-align: right;"></td>
</tr>
</tbody></table>
	
	
</div>

<div class="viborka3" style="display: none;">
<p><span><?=$arResult['NAME']?></span> <span id="tovar_name1">Оцинкованный</span> <span id="tovar_tolsh1">0.6мм</span></p>
<div class="calcnotice">Расчет стоимости.</div>
<table id="table_order">
<tbody><tr>
<td>№</td><td>Цвет</td><td>Длина листа</td><td>Количество листов</td><td>Количество погонных метров</td><td>Количество квадратных метров</td><td>Сумма</td>
</tr>


</tbody></table>
<table style="width: 100%">
<tbody><tr><td><a href="<?=$APPLICATION->GetCurPage()?>" id="viborka_back3" onclick="return false;"><div class="button" style="position: relative;">Назад<img src="/images/strel_button2.png" style="position:absolute;  height: 100%; left: 0; top: 0;"></div></a><br></td><td></td><td>
<a href="<?=$APPLICATION->GetCurPage()?>" data-href="<?=$arParams['BASKET_URL']?>" id="korzina_oform" onclick="return false;"><div class="button">Перейти к оформлению</div></a>
<a href="<?=$APPLICATION->GetCurPage()?>" id="korzina_prod" onclick="return false;"><div class="button">Положить в корзину и продолжить</div></a>
<a id="bistr_zayavka" class="fancybox" href="#block_bistr_zayav" onclick="return false;"><div class="button">Быстрая заявка</div></a></td></tr>
<tr><td></td><td></td><td></td></tr>
</tbody></table>


<form id="form_order_buy" action="/images/prof.htm" name="form_order_buy" method="post">
	
	<input type="hidden" name="tovar_order_bye" id="tovar_order_bye" value="3+Оцинкованный:Оцинкованный.0,6.2,00-2,00@265,65@1"> 
	<input type="hidden" name="type_order" id="type_order" value="">
	
</form>


		
		
</div>	


	
</div>
</div>



				<?endif;?>
                </td>
              </tr>
            </tbody></table>
            <table border="0" cellspacing="0" cellpadding="2" width="100%">
              <tbody>
			  <tr>
                <td valign="top" class="cd4"><?=$arResult['DETAIL_TEXT']?></td>
              </tr>
              <tr>
                <td class="cd5"></td>
              </tr>
              <tr>
                <td valign="bottom" height="20"><a href="javascript:history.back(1);"><?=GetMessage('BACK')?></a></td>
              </tr>
			  <?if(count($arResult['PRODUCTS']) > 0 && is_array($arResult['PRODUCTS'])):
			
			$GLOBALS['arrFilterProductProp']['ID'] = $arResult['PRODUCTS'];
		?>
		<tr>
		
		  <td valign="top" align="left" class="seealso">
			С этим товаром обычно заказывают это:
		  </td>
		</tr>


		<tr>
		  <td valign="top" align="left" class="seealso2">
			<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "recommend", array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => "",
			"SECTION_CODE" => "",
			"SECTION_USER_FIELDS" => array(
				0 => "",
			),
			"ELEMENT_SORT_FIELD" => "sort",
			"ELEMENT_SORT_ORDER" => "asc",
			"ELEMENT_SORT_FIELD2" => "id",
			"ELEMENT_SORT_ORDER2" => "desc",
			"FILTER_NAME" => "arrFilterProductProp",
			"INCLUDE_SUBSECTIONS" => "Y",
			"SHOW_ALL_WO_SECTION" => "Y",
			"HIDE_NOT_AVAILABLE" => "N",
			"PAGE_ELEMENT_COUNT" => "3",
			"LINE_ELEMENT_COUNT" => "3",
			"PROPERTY_CODE" => array(
				0 => "",
				1 => "",
			),
			"OFFERS_LIMIT" => "5",
			"SECTION_URL" => "/cat/#CODE#/",
			"DETAIL_URL" => "",
			"BASKET_URL" => "/personal/basket.php",
			"ACTION_VARIABLE" => "action",
			"PRODUCT_ID_VARIABLE" => "id",
			"PRODUCT_QUANTITY_VARIABLE" => "quantity",
			"PRODUCT_PROPS_VARIABLE" => "prop",
			"SECTION_ID_VARIABLE" => "SECTION_ID",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_GROUPS" => "Y",
			"META_KEYWORDS" => "-",
			"META_DESCRIPTION" => "-",
			"BROWSER_TITLE" => "-",
			"ADD_SECTIONS_CHAIN" => "N",
			"DISPLAY_COMPARE" => "N",
			"SET_TITLE" => "N",
			"SET_STATUS_404" => "N",
			"CACHE_FILTER" => "N",
			"PRICE_CODE" => array(
				0 => "BASE",
			),
			"USE_PRICE_COUNT" => "N",
			"SHOW_PRICE_COUNT" => "1",
			"PRICE_VAT_INCLUDE" => "N",
			"PRODUCT_PROPERTIES" => array(
			),
			"USE_PRODUCT_QUANTITY" => "N",
			"CONVERT_CURRENCY" => "N",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"PAGER_TITLE" => "Товары",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => "",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"AJAX_OPTION_ADDITIONAL" => ""
			),
			false
		);?>
		  
		  </td>
		</tr>
		<?endif;?>
            </tbody></table></td>
        </tr>
		
		
      </tbody></table></td>
  </tr>
 
</tbody></table>
</td>
</tr>
</tbody></table>