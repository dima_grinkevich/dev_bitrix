<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams['SMALL_PHOTO_WIDTH'] = intval($arParams['SMALL_PHOTO_WIDTH']);
if($arParams['SMALL_PHOTO_WIDTH'] <= 0)
	$arParams['SMALL_PHOTO_WIDTH'] = 200;
	
$arParams['SMALL_PHOTO_HEIGHT'] = intval($arParams['SMALL_PHOTO_HEIGHT']);
if($arParams['SMALL_PHOTO_HEIGHT'] <= 0)
	$arParams['SMALL_PHOTO_HEIGHT'] = 200;


if(is_array($arResult['DETAIL_PICTURE'])){
	$arPicture = $arResult['DETAIL_PICTURE'];
}elseif(is_array($arResult['PREVIEW_PICTURE'])){
	$arPicture = $arResult['PREVIEW_PICTURE'];
}
if(is_array($arPicture)){	
	$arResult['PICTURE'] =  array(
			'ID' => $arItem['DETAIL_PICTURE']['ID'],
			'ALT' => $arItem['DETAIL_PICTURE']['DESCRIPTION'],
			'BIG' => $arPicture,
			'SMALL' => resizeImage($arPicture, $arParams['SMALL_PHOTO_HEIGHT'], $arParams['SMALL_PHOTO_HEIGHT'])
	);
}
if(count($arResult['OFFERS'])){
	$arProp = $arProps = $arOffers = array();
/*/*/	$arKeys = array('COVER', 'COLOR', 'VIEW', 'THICKNESS');
	
	//взаимосвязь ID и цены
	$arResult['arPrices'] = array();
	
	//массив для получения ID предложения по параметрам
	$arResult['arOffers'] = array();
	$arResult['arOffers_COMPLEX'] = array(); // для комплексного
	
	$arResult['COMPLEX_ALGORITHM'] = strlen($arResult['PROPERTIES']['COMPLEX_ALGORITHM']['VALUE']) > 0 ? 'Y' : 'N';
	
	$arThickness = array();
	$arColorID = array();
	$arProp2 = array();
	$arLinks = array();
	foreach($arResult['OFFERS'] as $key => $arOffer){
		foreach($arOffer['DISPLAY_PROPERTIES'] as $key => $prop){
			if($prop['PROPERTY_TYPE'] == 'E' && $prop['LINK_IBLOCK_ID'] > 0){
				$rs = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $prop['LINK_IBLOCK_ID']));
				while($ar = $rs->Fetch()){
					$arLinks[$key][$ar['ID']] = $ar['NAME'];
				}
			}
		}
		break;
	}
	
	foreach($arResult['OFFERS'] as $key => $arOffer){
		$arOfferProp = $arOffer['PROPERTIES'];
		$arColorID[$arOfferProp['COLOR']['VALUE']] = $arOfferProp['COLOR']['VALUE'];
		if($arResult['COMPLEX_ALGORITHM'] != 'Y'){
			
			foreach($arOffer['PROPERTIES'] as $key => $prop){
				$value = $val = $prop['VALUE'] ? : '--';
				//if(strlen($prop['VALUE']) <=0 ) echo '<pre>'.print_r($prop ,true).'</pre>';
				$val = replaceSpace($val, false);
				$k = replaceSpace($key, false);

				$arProp2[$k]['NAME'] = $prop['NAME'];
				if($prop['PROPERTY_TYPE'] == 'E'){
					$name = $arLinks[$key][$prop['VALUE']] ? : '--';
				}else{
					$name = $value;
				}
				$arProp2[$k]['VALUES'][$val]['NAME'] = $name;
				
				foreach($arOffer['PROPERTIES'] as $key2 => $prop2){
					if($key == $key2) continue;
					$val2 = $prop2['VALUE'] ? : '--';
					$v = replaceSpace($key2, false).'_'.replaceSpace($val2, false);
					$arProp2[$k]['VALUES'][$val]['VALUES'][$v] = $v;
				}
				$arOffers[$arOffer['ID']]['ID'] = $arOffer['ID'];
				$arOffers[$arOffer['ID']][replaceSpace($key)] = replaceSpace($prop['VALUE'], false);
			}
			$arProps = array_merge($arProps, $arProp2);
/** old **/
			foreach($arKeys as $prop){
				if(strlen($arOfferProp[$prop]['VALUE']) <= 0)
					$arOfferProp[$prop]['VALUE'] = '--';
				
				$arProp[$prop]['NAME'] = $arOfferProp[$prop]['NAME'];
				//$arProp[$prop]['VALUES'][$arOfferProp[$prop]['VALUE']] = array();
				
				$arResult['PROPS'][ToLower($prop).'_'.$arOfferProp[$prop]['VALUE']] = ToLower($prop).'_'.$arOfferProp[$prop]['VALUE'];
				foreach($arKeys as $prop2){
					if($prop2 == $prop) continue;
					$val = $arOfferProp[$prop2]['VALUE'] ? : '--';
					$arProp[$prop]['VALUES'][$arOfferProp[$prop]['VALUE']][ToLower($prop2)][$val] = $val;
					$arResult['PROPS'][ToLower($prop).'_'.$arOfferProp[$prop]['VALUE']] .= ', .'.ToLower($prop2).'_'.$arOfferProp[$prop2]['VALUE'];
				}
			}
			
/*old*/
		}else{
			$arThickness[$arOfferProp['THICKNESS']['VALUE']] = $arOfferProp['THICKNESS']['VALUE'];
			$arResult['arOffers_COMPLEX'][$arOfferProp['COVER']['VALUE']][$arOfferProp['THICKNESS']['VALUE']][$arOfferProp['COLOR']['VALUE']] = $arOffer['ID'];
		}
		
		//свойства торгового предложения для js
		$arResult['arOffers'][replaceSpace($arOfferProp['COVER']['VALUE'])][replaceSpace($arOfferProp['COLOR']['VALUE'])][replaceSpace($arOfferProp['VIEW']['VALUE'])][replaceSpace($arOfferProp['THICKNESS']['VALUE'])] = $arOffer['ID'];
		
		//цена
		$arResult['arPrices'][$arOffer['ID']] = $arOffer['PRICES']['BASE']['VALUE'] > $arOffer['PRICES']['BASE']['DISCOUNT_VALUE'] ? $arOffer['PRICES']['BASE']['PRINT_DISCOUNT_VALUE'] : $arOffer['PRICES']['BASE']['PRINT_VALUE'];
		
		unset($arOfferProp);
	}
	
	$arResult['OFFERS_PROPS'] = $arProps;
	$arResult['OFFERS_2'] = $arOffers;
	//цвета предложений
	$arResult['OFFER_COLORS'] = array();
	$arResult['COLORS'] = array();
	$rsColor = CIBlockElement::GetList(array(), array('ID' => $arColorID), false, false, array('ID', 'NAME', 'PREVIEW_PICTURE', 'PROPERTY_NUMBER_COLOR'));
	while($arColor = $rsColor->Fetch()){
		$arResult['OFFER_COLORS'][$arColor['ID']] = $arColor;
		$arResult['OFFER_COLORS_INV'][$arColor['NAME']] = $arColor['ID'];
		$arResult['COLORS'][] = $arColor['NAME'];
		$arResult['COLORS'][] = $arColor['PROPERTY_NUMBER_COLOR_VALUE'];
	}
	//echo '<pre>'.print_r( $arResult['COLORS'],true).'</pre>';
	if($arResult['COMPLEX_ALGORITHM'] != 'Y'){
		//параметры уникальные предложений
		$arResult['OFFERS_PROP'] = $arProp;
	}else{
		sort($arThickness);
		$arResult['THICKNESS_SORT'] = $arThickness;
	}
	
}

//echo '<pre>'.print_r($arResult['OFFERS_PROPS'] ,true).'</pre>';
$arResult['PRODUCTS'] = $arResult['PROPERTIES']['PRODUCTS']['VALUE'];
$this->__component->arResultCacheKeys = array_merge($this->__component->arResultCacheKeys, array('PRODUCTS'));
?>
