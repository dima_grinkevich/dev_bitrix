$(document).ready(function(){
	var cost = $('#cost'),
		inputs = $('.prop_price')
		item = $('#item');
	
	function setDefaultOffer(){
		if(oOffers2 != undefined){
			for(var k in oOffers2){
				inputs.each(function(){
					var inp = $(this);
					inp.val(oOffers2[k][inp.data('code')]);
				});
				result = k;
				break;
			}
			
			setPrice(result);
		}
	}
	$('.reset_select').click(function(){
		$('option', inputs).removeClass('_hide');
		setDefaultOffer();
	});
		
	$('.prop_price').change(function(){
		var click = $(this);
		var classSelect = click.data('code')+'_'+click.val();
		var result = '';
		$('.reset_select').show();
		
		$('option', inputs).addClass('_hide');
		$('option.'+classSelect, inputs).removeClass('_hide');
		
		$('[value="'+click.val()+'"]', click).removeClass('_hide');
		
		
		
		inputs.each(function(){
			
			var input = $(this);
			
			var option = $('option[value="'+input.val()+'"]', input).not('._hide');
			if(option.length <= 0){
				
				var el = $('option.'+classSelect, input).not('._hide');
				if(el.length > 1){
					if(el.eq(0).val() != '--') input.val(el.eq(0).val());
					else input.val(el.eq(1).val());
					
				}else{
					input.val(el.eq(0).val());
				}
				

				
				
			}
			classSelect += '.'+input.data('code')+'_'+input.val();
			//val = val.toString();
			
			//result = result[val];
			
		});

		for(var key in oOffers2) {
			var bFlg = true;
			inputs.each(function(){
				var inp = $(this);
				
				if(oOffers2[key][inp.data('code')] != inp.val()){
					bFlg = false;
				}
			});
			if(bFlg){
				result = oOffers2[key]['ID'];
				break;
			}
		}

		if(result == '')
			for(var key in oOffers2) {
				result = key;
				break;
			}
		setPrice(result);
	});
	
	function setPrice(result){
		item.val(result);
		cost.html(oPrices[parseInt(result)]);
	}
	setDefaultOffer();
});