<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"]) < 1): return false; endif;
$arParams['SLIDER_ID'] = strlen($arParams['SLIDER_ID']) > 0 ? $arParams['SLIDER_ID'] : 'b-items-slider';
$arParams['DISPLAY_DURATION'] = intval($arParams['DISPLAY_DURATION']) > 0 ? $arParams['DISPLAY_DURATION'] : 2000;
?>
	<ul class="list-item" id="b-items-slider">
		<?foreach($arResult["ITEMS"] as $arElement):
			$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arElement["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arElement["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			$bHasPicture = is_array($arElement['PREVIEW_IMG']);
		?>
			<?if($arParams["DISPLAY_PICTURE"]!="N" && $bHasPicture):?>
				<li class="item" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
					<?$slide= '';?>
					<?if(strlen($arElement['PREVIEW_TEXT']) > 0):?>
					<?
						$slide .= '<a href="'.$arElement['~PREVIEW_TEXT'].'"'; 
					else:
						$slide .= '<span';
					endif;?>
					<?
					$slide .= ' style="background-image: url('.$arElement['PREVIEW_IMG']['SRC'].')">';
					?>
					<?if(strlen($arElement['PREVIEW_TEXT']) > 0):
						$slide .= '</a>';
					else:
						$slide .= '</span>';
					endif;?>
					
					
					<?echo $slide?>

					<?/*if(strlen($arElement["DETAIL_TEXT"]) > 0):?>
						<div class="mask">
							 <?if(strlen($arElement['PREVIEW_TEXT']) > 0):?>
								<a href="<?=$arElement['PREVIEW_TEXT'];?>" class="title">
							 <?else:?>
								<span class="title">
							 <?endif;?>
								<?=$arElement["NAME"]?>
							<?if(strlen($arElement['PREVIEW_TEXT']) > 0):?>
								</a>
							<?else:?>
								</span>
							 <?endif;?>
							<?if($arElement['DETAIL_TEXT_TYPE'] == 'html'):?><?=$arElement["DETAIL_TEXT"]?><?else:?><p><?=$arElement["DETAIL_TEXT"]?></p><?endif;?>
						</div>
						<?endif;*/?>
				</li>
			<?endif;?>
		<?endforeach;?>
	</ul>
	<div class="pagination" id="b-items-slider_pag"></div>

<script>
$(function(){

$(window).load(function(){
    $("#b-items-slider").carouFredSel({
        width: '100%',
        circular: false,
        infinite: true,
        item: {
            width: <?=$arParams['PHOTO_WIDTH']?>
        },
        auto 	:  4000,
        scroll:{
            duration: <?=$arParams['DISPLAY_DURATION']?>,
            pauseOnHover: true
        },
        pagination	: "#b-items-slider_pag"
    });
});

});
<?/*/?>
$(window).load(function(){
    $("#b-items-slider").carouFredSel({
        circular: false,
        infinite: true,
        auto 	:  <?=$arParams['DISPLAY_DURATION']?>,
        items:{
                visible: 1,
                height: <?=$arParams['DISPLAY_IMG_HEIGHT']?>,
                width: <?=$arParams['DISPLAY_IMG_WIDTH']?>
            },
		scroll: {
			items: 1,
			duration: 2000,
			fx: 'scroll',
			pauseOnHover: true
		},
        pagination	: "#b-items-slider_pag"
    });
});<?//*/?>
</script>

<?/*/?><pre><?print_r($arParams)?></pre><?//*/?>