<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$arParams['PHOTO_WIDTH'] = intval($arParams['PHOTO_WIDTH']);
if($arParams['PHOTO_WIDTH'] <= 0)
	$arParams['PHOTO_WIDTH'] = 1000;
	
$arParams['PHOTO_HEIGHT'] = intval($arParams['PHOTO_HEIGHT']);
if($arParams['PHOTO_HEIGHT'] <= 0)
	$arParams['PHOTO_HEIGHT'] = 100;

function resizeImage($id, $width, $height){
	$arResizeFile = CFile::ResizeImageGet(
			 $id, 
			array("width"=>$width, "height"=>$height), 
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT, 
			true, 
			false, 
			false, 
			100
		);
		
	return array(
		'SRC' => $arResizeFile['src'],
		'WIDTH' => $arResizeFile['width'],
		'HEIGHT' => $arResizeFile['height']
	);
}

foreach($arResult['ITEMS'] as $key => $arItem){
	if(is_array($arItem['PREVIEW_PICTURE'])){
		$arResult['ITEMS'][$key]['PREVIEW_IMG'] = resizeImage($arItem['PREVIEW_PICTURE'], $arParams['PHOTO_WIDTH'], $arParams['PHOTO_HEIGHT']);
	}
}