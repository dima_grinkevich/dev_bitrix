<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_DURATION" => Array(
		"NAME" => GetMessage('DISPLAY_DURATION'),
		"TYPE" => "STRING",
		"DEFAULT" => 2000,
	),
	"PHOTO_WIDTH" => Array(
		"NAME" => GetMessage('PHOTO_WIDTH'),
		"TYPE" => "STRING",
		"DEFAULT" => 1000,
	),
	"PHOTO_HEIGHT" => Array(
		"NAME" => GetMessage('PHOTO_HEIGHT'),
		"TYPE" => "STRING",
		"DEFAULT" => 100,
	),
	
);
?>