<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixBasketComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */
use Bitrix\Main\Loader;
use Bitrix\Sale\DiscountCouponsManager;

if (!Loader::includeModule("sale") )
{
	ShowError(GetMessage("SOA_MODULE_NOT_INSTALL"));
	return;
}

$arParams["ORDER_ID"] = intval($arParams["ORDER_ID"]);
$arParams["FUSER"] = CSaleBasket::GetBasketUserID();

$arResult = array();
$arResult["ITEMS"] = array();
if($arParams["ORDER_ID"] <= 0) return;

Loader::includeModule("catalog");
Loader::includeModule("iblock");

$arResult["ORDER"] = CSaleOrder::GetByID($arParams["ORDER_ID"]);
$arResult["ORDER_PROPS"] = array();

$db_vals = CSaleOrderPropsValue::GetList(array("SORT" => "ASC"),array("ORDER_ID" => $arParams["ORDER_ID"]));
while($arVals = $db_vals->Fetch()){
	$arResult["ORDER_PROPS"][$arVals["CODE"]] = $arVals;
}

$arResult["DELIVERY"] = CSaleDelivery::GetByID($arResult["ORDER"]["DELIVERY_ID"]);
$arResult["PAY_SYSTEM"] = CSalePaySystem::GetByID($arResult["ORDER"]["PAY_SYSTEM_ID"]);
$arCurrency = CCurrencyLang::GetByID($arResult["ORDER"]['CURRENCY'], LANGUAGE_ID);
$currencyThousandsSep = (!$arCurrency["THOUSANDS_VARIANT"] ? $arCurrency["THOUSANDS_SEP"] : ($arCurrency["THOUSANDS_VARIANT"] == "S" ? " " : ($arCurrency["THOUSANDS_VARIANT"] == "D" ? "." : ($arCurrency["THOUSANDS_VARIANT"] == "C" ? "," : ($arCurrency["THOUSANDS_VARIANT"] == "B" ? "\xA0" : "")))));

$arSelFields = array("ID", "PRODUCT_ID", "QUANTITY", "CAN_BUY", "PRICE", "WEIGHT", "NAME", "CURRENCY", "DISCOUNT_PRICE", "TYPE", "SET_PARENT_ID", "DETAIL_PAGE_URL");
$resBasketItems = CSaleBasket::GetList(array('SORT' => 'DESC'), array('ORDER_ID' => $arParams["ORDER_ID"]), false, false, $arSelFields);


$arIDs = array();
$arSendItems = array();

while($arBasketItem = $resBasketItems->Fetch()){
	
	if(CSaleBasketHelper::isSetItem($arBasketItem)) // set item
		continue;

	if($arBasketItem['CAN_BUY'] === 'Y'){
		$curPrice = roundEx($arBasketItem['PRICE'], SALE_VALUE_PRECISION) * DoubleVal($arBasketItem['QUANTITY']);
		$orderPrice += $curPrice;
		
		$arIDs[] = $arBasketItem["PRODUCT_ID"];
		
		$arSendItems[$arBasketItem["PRODUCT_ID"]] = array(
			"ID" => $arBasketItem["PRODUCT_ID"],
			"NAME" => $arBasketItem['NAME'],
			"ITEM_PRICE" => str_replace('#', number_format($arBasketItem['PRICE'], $arCurrency["DECIMALS"], $arCurrency["DEC_POINT"], $currencyThousandsSep), $arCurrency['FORMAT_STRING']),
			"QTY" => intval($arBasketItem['QUANTITY']),
			"ITEM_TOTAL" =>  str_replace('#', number_format($curPrice, $arCurrency["DECIMALS"], $arCurrency["DEC_POINT"], $currencyThousandsSep), $arCurrency['FORMAT_STRING']),
		);
	}
	
	$arResult["ORDER_PRICE"] = str_replace('#', number_format($orderPrice, $arCurrency["DECIMALS"], $arCurrency["DEC_POINT"], $currencyThousandsSep), $arCurrency['FORMAT_STRING']);
}
if($arIDs){

	$arIblockItems = array();
	$rs = CIBlockElement::GetList(array(), array("ID" => $arIDs), false, false, array("ID", "PREIVEW_PICTURE", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_CML2_ARTICLE"));
	while($item = $rs->GetNext()){
		
		$picture = $item["PREIVEW_PICTURE"] ? $item["PREIVEW_PICTURE"] : $item["DETAIL_PICTURE"] ? $item["DETAIL_PICTURE"] : false;
		if($picture){
			$picture = resizeImage($picture, 50, 50);
		}
		
		$arSendItems[$item["ID"]] = array_merge(
			$arSendItems[$item["ID"]], 
			array(
				"URL" => $item["DETAIL_PAGE_URL"],
				"PICTURE" => $picture ? $picture : "",
				"ARTNUMBER" => $item["PROPERTY_CML2_ARTICLE_VALUE"]
			)
		);
		
	}
	unset($picture, $item);
	
	$arResult["ITEMS"] = $arSendItems;

}
$arResult["ORDER"]["PRICE_DELIVERY"] = str_replace('#', number_format($arResult["ORDER"]["PRICE_DELIVERY"], $arCurrency["DECIMALS"], $arCurrency["DEC_POINT"], $currencyThousandsSep), $arCurrency['FORMAT_STRING']);
$arResult["ORDER"]["PRICE_FORMAT"] = str_replace('#', number_format($arResult["ORDER"]["PRICE"], $arCurrency["DECIMALS"], $arCurrency["DEC_POINT"], $currencyThousandsSep), $arCurrency['FORMAT_STRING']);

$this->IncludeComponentTemplate();