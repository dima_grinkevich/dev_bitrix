<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(count($arResult["ITEMS"]) <= 0) return;

?>
Данные покупателя:
<br>
<br>
<table border="1px solid black" width="100%" style="border-collapse:collapse;" >
	<tr>
		<td style='padding: 4px;' >ФИО</td>
		<td style='padding: 4px;' ><?=$arResult["ORDER_PROPS"]["FIO"]["VALUE"]?></td>
	</tr>
	<tr>
		<td style='padding: 4px;' >email</td>
		<td style='padding: 4px;' ><?=$arResult["ORDER_PROPS"]["EMAIL"]["VALUE"]?></td>
	</tr>
	
	<tr>
		<td style='padding: 4px;' >Телефон</td>
		<td style='padding: 4px;' ><?=$arResult["ORDER_PROPS"]["PHONE"]["VALUE"]?></td>
	</tr>	
	<tr>
		<td style='padding: 4px;' >Регион Доставки</td>
		<td style='padding: 4px;' ><?=$arResult["ORDER_PROPS"]["CITY"]["VALUE"]?></td>
	</tr>	
	<tr>
		<td style='padding: 4px;' >Адрес Доставки</td>
		<td style='padding: 4px;' ><?=$arResult["ORDER_PROPS"]["ADDRESS"]["VALUE"]?></td>
	</tr>	
	<tr>
		<td style='padding: 4px;' >Способ доставки</td>
		<td style='padding: 4px;' ><?=$arResult["DELIVERY"]["NAME"]?></td>
	</tr>
	<tr>
		<td style='padding: 4px;' >Стоимость доставки</td>
		<td style='padding: 4px;' ><?=$arResult["ORDER"]["PRICE_DELIVERY"]?></td>
	</tr>
	<tr>
		<td style='padding: 4px;' >Способ оплаты</td>
		<td style='padding: 4px;' ><?=$arResult["PAY_SYSTEM"]["NAME"]?></td>
	</tr>	
	<tr>
		<td style='padding: 4px;' >Комментарий</td>
		<td style='padding: 4px;' ><?=$arResult["ORDER"]["COMMENTS"]?></td>
	</tr>
	
</table>
<br>

<p>Список товаров</p>
<span id="content_send"></span>
<table border="1px solid black" width="100%" style="border-collapse:collapse;" >
	<tr><th style="padding: 4px;" >Фото
		</th>
		<th style="padding: 4px;" >Наименование
		</th>
		<th style="padding: 4px" >Кол-во
		</th>
		<th style="padding: 4px" >Цена
		</th>
		<th style="padding: 4px" >Сумма
		</th>
		<th style="padding: 4px" >Артикул
		</th>
	</tr>
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<tr>
		<td style='padding: 4px;text-align: center;' ><a href='<?=$arItem["URL"]?>'><img src ="<?=$arItem["PICTURE"]["SRC"]?>" width='<?=$arItem["PICTURE"]["WIDTH"]?>' height ='<?=$arItem["PICTURE"]["HEIGHT"]?>'/></a>
		</td><td style='padding: 4px;text-align: center;' > <a href='<?=$arItem["URL"]?>'><?=$arItem["NAME"]?></a>
		</td>
		<td style='padding: 4px;text-align: center;' ><?=$arItem["QTY"]?>
		</td>
		<td style='padding: 4px;text-align: center;' ><?=$arItem["ITEM_PRICE"]?>
		</td>
		<td style='padding:4px;text-align: center;' ><?=$arItem["ITEM_TOTAL"]?>
		</td>
		<td style='padding: 4px;text-align: center;' ><?=$arItem["ARTNUMBER"]?>
		</td>
	</tr>
	<?echo "\n";?>
	<?endforeach?>
	
</table>
<p style="width: 204px;float: right;font-size: 18px;">
Итого : <?=$arResult["ORDER"]["PRICE_FORMAT"]?>
</p>