<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CJSCore::Init(array('ajax', 'popup'));

if(\Bitrix\Main\Loader::includeModule("socialservices"))
{
    $arSettingSocial = array(
        'Facebook' => array(
            'FUNCTION' => 'ballsSharingFb',
            'SCRIPT' => '//connect.facebook.net/en_US/sdk.js'
        ),
        'VKontakte' => array(
            'FUNCTION' => 'ballsSharingVk',
            'SCRIPT' => '//vk.com/js/api/openapi.js'
        ),
        'OK' => array(
            'FUNCTION' => 'ballsSharingOk',
            'SCRIPT' => '//api.ok.ru/js/fapi5.js'
        )
    );

    $obSocServ = new CSocServAuthManager();

    foreach($obSocServ->GetActiveAuthServices() as $arService){

        if(array_key_exists($arService['ID'], $arSettingSocial)){
            $classService = $arService['CLASS'];
            if(class_exists($classService)){
                $oService = new $classService();
                $appId = $oServiceOAuth = $oService->getEntityOAuth()->getAppID();

                if(strlen($appId) > 0){
                    $arResult['SOCIAL'][$arService['ID']] = array_merge(
                        $arSettingSocial[$arService['ID']],
                        array(
                            'ONCLICK' => $arSettingSocial[$arService['ID']]['FUNCTION'].'('.$appId.'); return false;'
                        )
                    );

                    if(strlen($arSettingSocial[$arService['ID']]['SCRIPT']) > 0){
                        \Bitrix\Main\Page\Asset::getInstance()->addJs($arSettingSocial[$arService['ID']]['SCRIPT']);
                    }
                }
            }
        }
    }

    $arResult['AJAX_FOLDER'] = str_replace($_SERVER['DOCUMENT_ROOT'], '', __DIR__).'/ajax.php';
}
$this->IncludeComponentTemplate();
