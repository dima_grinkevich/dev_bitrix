<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("GDS_BALLS_SHARING"),
	"DESCRIPTION" => GetMessage("GDS_BALLS_SHARING"),
	//"ICON" => "/images/comp_result_new.gif",
	//"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "balls",
		"NAME" => GetMessage("GDS_BALLS_MODULE"),
		"CHILD" => array(
			"ID" => "balls.sharing",
			"NAME" => GetMessage("GDS_BALLS_SHARING"),
			"SORT" => 20,
		)
	),
);
?>
