<?php

	use OpCode\ORM\Bitrix;

	CModule::IncludeModule('catalog');

	AddEventHandler('catalog', 'OnCondCatControlBuildList', ['ocCatalogDiscountConditions', 'GetControlDescr'], 0);
	AddEventHandler('sale', 'OnCondSaleActionsControlBuildList', ['ocCatalogDiscountConditions', 'GetControlDescr'], 0);
	AddEventHandler('sale', 'OnExtendBasketItems', ['ocCatalogDiscountConditions', 'OnExtendBasketItems']);
	AddEventHandler('sale', 'OnOrderAdd', ['ocCatalogDiscountConditions', 'OnOrderAdd'], 0);
	
	class ocCatalogDiscountConditions extends CGlobalCondCtrlComplex
	{
		public static $iCurrentOrderID = false;

		public static function GetClassName() {
			return __CLASS__;
		}

//		public static function GetControlID() {
//			return [
//				'ocConditionControl_isFirstWork',
//				'ocConditionControl_basketItemIndex',
//				'ocConditionControl_promoOrderIndex'
//			];
//		}

		public static function GetControlDescr()
		{
			$className = static::GetClassName();
			return [
				'COMPLEX' => 'Y',
				'GetControlShow' => [$className, 'GetControlShow'],
				'GetConditionShow' => [$className, 'GetConditionShow'],
				'IsGroup' => [$className, 'IsGroup'],
				'Parse' => [$className, 'Parse'],
				'Generate' => [$className, 'Generate'],
				'ApplyValues' => [$className, 'ApplyValues'],
				'InitParams' => [$className, 'InitParams'],
				'CONTROLS' => static::GetControls()
			];
		}

		public static function GetControls($sControlID = false) {
			$arControls = [
				'ocConditionControl_isFirstWork' => [
					'ID' => 'ocConditionControl_isFirstWork',
					'PARENT' => true,
					'FIELD' => 'OPCODE_FIELD',
					'FIELD_TYPE' => 'int',
					'MULTIPLE' => 'N',
					'GROUP' => 'Y',
					'LABEL' => 'Кол-во работ на конкурсе',
					'PREFIX' => 'Кол-во работ на конкурсе',
					'LOGIC' => static::GetLogic([BT_COND_LOGIC_EQ, BT_COND_LOGIC_NOT_EQ, BT_COND_LOGIC_GR, BT_COND_LOGIC_LS, BT_COND_LOGIC_EGR, BT_COND_LOGIC_ELS]),
					'JS_VALUE' => [
						'type' => 'input'
					],
					'PHP_VALUE' => ''
				],
/*
				'ocConditionControl_basketItemIndexThrough' => [
					'ID' => 'ocConditionControl_basketItemIndexThrough',
					'PARENT' => true,
					'FIELD' => 'OPCODE_NOMINATION_INDEX_THROUGHT',
					'FIELD_TYPE' => 'int',
					'MULTIPLE' => 'N',
					'GROUP' => 'Y',
					'LABEL' => 'Порядковый номер неспонсорской номинации во всех работах',
					'PREFIX' => 'Порядковый номер неспонсорской номинации во всех работах',
					'LOGIC' => static::GetLogic(array(BT_COND_LOGIC_EQ, BT_COND_LOGIC_NOT_EQ, BT_COND_LOGIC_GR, BT_COND_LOGIC_LS, BT_COND_LOGIC_EGR, BT_COND_LOGIC_ELS)),
					'JS_VALUE' => array(
						'type' => 'input'
					),
					'PHP_VALUE' => ''
				],
*/
				'ocConditionControl_basketItemIndex' => [
					'ID' => 'ocConditionControl_basketItemIndex',
					'PARENT' => true,
					'FIELD' => 'OPCODE_BASKET_ITEM_INDEX',
					'FIELD_TYPE' => 'int',
					'MULTIPLE' => 'N',
					'GROUP' => 'Y',
					'LABEL' => 'Порядковый номер неспонсорской номинации в корзине',
					'PREFIX' => 'Порядковый номер неспонсорской номинации в корзине',
					'LOGIC' => static::GetLogic([BT_COND_LOGIC_EQ, BT_COND_LOGIC_NOT_EQ, BT_COND_LOGIC_GR, BT_COND_LOGIC_LS, BT_COND_LOGIC_EGR, BT_COND_LOGIC_ELS]),
					'JS_VALUE' => [
						'type' => 'input'
					],
					'PHP_VALUE' => ''
				],
				'ocConditionControl_promoOrderIndex' => [
					'ID' => 'ocConditionControl_promoOrderIndex',
					'PARENT' => true,
					'FIELD' => 'OPCODE_PROMO_ORDER_INDEX',
					'FIELD_TYPE' => 'int',
					'MULTIPLE' => 'N',
					'GROUP' => 'Y',
					'LABEL' => 'Порядковый номер заказа с промокодом',
					'PREFIX' => 'Порядковый номер заказа с промокодом (нумерация с 1)',
					'LOGIC' => static::GetLogic([BT_COND_LOGIC_EQ, BT_COND_LOGIC_NOT_EQ, BT_COND_LOGIC_GR, BT_COND_LOGIC_LS, BT_COND_LOGIC_EGR, BT_COND_LOGIC_ELS]),
					'JS_VALUE' => [
						'type' => 'input'
					],
					'PHP_VALUE' => ''
				],
				'ocConditionControl_sponsoredCount' => [
					'ID' => 'ocConditionControl_sponsoredCount',
					'PARENT' => true,
					'FIELD' => 'OPCODE_SPONSORED_COUNT',
					'FIELD_TYPE' => 'int',
					'MULTIPLE' => 'N',
					'GROUP' => 'Y',
					'LABEL' => 'Кол-во спонсорских номинаций в корзине',
					'PREFIX' => 'Кол-во спонсорских номинаций в корзине',
					'LOGIC' => static::GetLogic([BT_COND_LOGIC_EQ, BT_COND_LOGIC_NOT_EQ, BT_COND_LOGIC_GR, BT_COND_LOGIC_LS, BT_COND_LOGIC_EGR, BT_COND_LOGIC_ELS]),
					'JS_VALUE' => [
						'type' => 'input'
					],
					'PHP_VALUE' => ''
				],
				'ocConditionControl_goldenCount' => [
					'ID' => 'ocConditionControl_goldenCount',
					'PARENT' => true,
					'FIELD' => 'OPCODE_GOLDEN_COUNT',
					'FIELD_TYPE' => 'int',
					'MULTIPLE' => 'N',
					'GROUP' => 'Y',
					'LABEL' => 'Кол-во золотых номинаций в корзине',
					'PREFIX' => 'Кол-во золотых номинаций в корзине',
					'LOGIC' => static::GetLogic([BT_COND_LOGIC_EQ, BT_COND_LOGIC_NOT_EQ, BT_COND_LOGIC_GR, BT_COND_LOGIC_LS, BT_COND_LOGIC_EGR, BT_COND_LOGIC_ELS]),
					'JS_VALUE' => [
						'type' => 'input'
					],
					'PHP_VALUE' => ''
				]
			];

			return $sControlID ? $arControls[$sControlID] : $arControls;
		}

		public static function SetCurrentOrder($iOrderID) {
			static::$iCurrentOrderID = $iOrderID;
		}

		public static function OnOrderAdd($iOrderID, $arOrder) {
			static::SetCurrentOrder($iOrderID);
		}

		public static function OnExtendBasketItems(&$arBasketContent) {
			/** @var $GLOBALS cUser[] */
			$iOrderCount = 0;
			$iOrderWithCouponIndex = 0;
			$iBasketItemIndex = 1;
			$iBasketItemIndexThrought = 0;
			$iBasketCounters = [
				'sponsored' => 0,
				'golden' => 0
			];

			/* в ajax.php есть ORDER_ID => null */

			$arOrders = Bitrix::create('SaleOrder')
				->setFilter(['USER_ID' => $GLOBALS['USER']->GetID()])
				->getElements();

			foreach ($arOrders as $arOrder) {
				if (static::$iCurrentOrderID && $arOrder['ID'] == static::$iCurrentOrderID)
					/* Рассчёт всегда проводим с учётом только предыдущей истории, так,
					 * буд-то мы всегда находимся в точке времени в которой создавался заказ.
					 * Это нужно для корректной работы перерасчёта заказа в будущем */
					break;

				$iOrderCount++;
				$iBasketItemIndexThrought += CSaleBasket::GetList([], ['ORDER_ID' => $arOrder['ID']], []);
				if ($iOrderWithCouponIndex == 0 && $arOrder['USER_DESCRIPTION'])
					$iOrderWithCouponIndex = $iOrderCount;
			}

			$arProducts = []; foreach ($arBasketContent as $arProduct) {
				$arElement = Bitrix::create()
					->setFilter(['ID' => $arProduct['PRODUCT_ID']])
					->setSelect(['ID', 'IBLOCK_SECTION_ID'])
					->getElement();

				if ($arElement['IBLOCK_SECTION_ID'] == CFG_SPONSORED_SECTION_ID) $iBasketCounters['sponsored']++;
				if ($arElement['IBLOCK_SECTION_ID'] == CFG_GOLDEN_SECTION_ID) $iBasketCounters['gold']++;

				$arProducts[$arProduct['PRODUCT_ID']] = $arElement;
			}

			foreach ($arBasketContent as &$arProduct) {
				$arProduct['OPCODE_FIELD'] = $iOrderCount;
				$arProduct['OPCODE_PROMO_ORDER_INDEX'] = $iOrderWithCouponIndex;
				$arProduct['OPCODE_SPONSORED_COUNT'] = $iBasketCounters['sponsored'];
				$arProduct['OPCODE_GOLDEN_COUNT'] = $iBasketCounters['golden'];
 				$arProduct['OPCODE_NOMINATION_INDEX_THROUGHT'] = $iBasketItemIndexThrought;
				if ($arProducts[$arProduct['PRODUCT_ID']]['IBLOCK_SECTION_ID'] != CFG_SPONSORED_SECTION_ID)
					$arProduct['OPCODE_BASKET_ITEM_INDEX'] = $iBasketItemIndex++;
			}
		}

		public static function GetControlShow($arParams) {
			$arResult = [
				'controlgroup' => true,
				'group' => false,
				'label' => 'Кастомные условия предоставления скидки',
				'showIn' => static::GetShowIn($arParams['SHOW_IN_GROUPS']),
				'children' => array()
			];

			foreach (static::GetControls() as $arControl) {
				$arLogic = static::GetLogicAtom($arControl['LOGIC']);
				$arValue = static::GetValueAtom($arControl['JS_VALUE']);
				$arResult['children'][] = array(
					'controlId' => $arControl['ID'],
					'group' => false,
					'label' => $arControl['LABEL'],
					'showIn' => static::GetShowIn($arParams['SHOW_IN_GROUPS']),
					'control' => array(
						array(
							'id' => 'prefix',
							'type' => 'prefix',
							'text' => $arControl['PREFIX']
						),
						$arLogic,
						$arValue
					)
				);
			}

			return $arResult;
		}

		public static function Generate($arOneCondition, $arParams, $arControl, $arSubs = false) {
			return '$row'.parent::Generate($arOneCondition, $arParams, $arControl, $arSubs);
		}
	}
