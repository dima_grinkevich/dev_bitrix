<?

AddEventHandler("main", "OnBeforeUserRegister", Array("GDS_EventsUser", "OnBeforeUserRegister"));
AddEventHandler("main", "OnAfterUserRegister", Array("GDS_EventsUser", "OnAfterUserRegister"));
AddEventHandler("main", "OnAfterUserUpdate", Array("GDS_EventsUser", "OnAfterUserUpdate"));


Class GDS_EventsUser
{
	//����� ������������
	function OnBeforeUserRegister(&$arFields)
    {
		$arFields["LOGIN"] = $arFields["EMAIL"]; // ��� ����������� ����� ������ email
    } 
	
	//����� �����������
	function OnAfterUserRegister(&$arFields)
    {
		if($arFields['USER_ID'] > 0){
			//-start- ������� ���� ��� ��
			$arFieldsAccount = Array("USER_ID" => $arFields['USER_ID'], "CURRENCY" => "RUB", "CURRENT_BUDGET" => 0);
			$accountID = CSaleUserAccount::Add($arFieldsAccount);
			// -end-
			
			//-start- �������� �� �������� "�������� �������" [UF_NOTIFY]
			if(strlen($arFields['UF_NOTIFY']) > 0){
				self::SubscriptionsUser($arFields['USER_ID'], $arFields['EMAIL']);
			}else{
				self::UnSubscriptionsUser($arFields['USER_ID'], $arFields['EMAIL']);
			}
			// -end-
		}
		//CUser::Delete($arFields['USER_ID']);
    }
	
	//����� ���������� ������������
	function OnAfterUserUpdate($arFields){
	
		if(intval($arFields['UF_NOTIFY']) > 0){
			self::SubscriptionsUser($arFields['ID'], $arFields['EMAIL']);
		}else{
			self::UnSubscriptionsUser($arFields['ID'], $arFields['EMAIL']);
		}
	}
	
	// ����������� ������������ �� USER_ID � email �� ��� ��������
	static function SubscriptionsUser($iUserId, $sEmail = false){
		CModule::IncludeModule('subscribe');
		$arRubric = array();
		$rs = CRubric::GetList(array(), array('ACTIVE' => 'Y'));    
		
		while($ar = $rs->Fetch()){
			$arRubric[] = $ar['ID'];
		}
		if(count($arRubric) > 0){
			if(!check_email($sEmail)){
				$rsUser = CUser::GetById($iUserId);
				if($arUser = $rs->Fetch()){
					$sEmail = $arUser['EMAIL'];
				}
			}
			$subscr = new CSubscription;
			$arFields = array(
				'USER_ID' => $iUserId,
				'ACTIVE' => 'Y',
				'EMAIL' => $sEmail,
				'FORMAT' => 'html',
				'CONFIRMED' => 'Y',
				'RUB_ID' => $arRubric,
				'SEND_CONFIRM' => 'N'
			);
			$subscr->Add($arFields);
		}
	}
	
	// ���������� ������������ �� USER_ID � email �� ��� ��������
	static function UnSubscriptionsUser($iUserId, $sEmail = false){
		CModule::IncludeModule('subscribe');
		
		$arSubscription = array();
		if(check_email($sEmail)){
			$arFilter = array('EMAIL' => $sEmail);
		}else{
			$arFilter = array('ID' => $iUserId);
		}
		$rs = CSubscription::GetList(false, $arFilter);
		while($ar = $rs->Fetch()){
			CSubscription::Delete($ar['ID']);
		}
	}
}