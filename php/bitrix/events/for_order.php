<?
AddEventHandler("sale", "OnOrderNewSendEmail", "OnOrderNewSendEmailCustom");
function OnOrderNewSendEmailCustom($nID, &$eventName,&$arFields){
	global $APPLICATION;
	ob_start();
	$APPLICATION->IncludeComponent(
		"gds:order.send",
		"",
		Array(
			"ORDER_ID" => $nID
		)
	);
	$body = ob_get_contents();

	ob_end_clean();

	$arAdminFields = array(
		"ORDER_ID" => $nID,
		"BODY" => $body
	);

	$rs = CEvent::SendImmediate("SALE_ORDER_ADMIN", SITE_ID, $arAdminFields);
	$arFields["ORDER_LIST"] = substr($body, strripos($body, "content_send")+14, strlen($body));

}

AddEventHandler("sale", "OnOrderNewSendEmail", "ChangingEmailTemplate");
function ChangingEmailTemplate($orderId, &$eventName, &$arFields){
	if(!empty($arFields)){

		$arOrder = CSaleOrder::GetByID($orderId);

		$delivery = CSalePersonType::GetByID($arOrder['PERSON_TYPE_ID']);

		//-- �������� �������� ������
		$order_props = CSaleOrderPropsValue::GetOrderProps($orderId);
		$strDop = '';
		while ($arProps = $order_props->Fetch())
		{
			$arProps["VALUE"] = htmlspecialchars($arProps["VALUE"]);
			if ($arProps["CODE"] == "PHONE")
			{
				$phone = $arProps["VALUE"];
			}elseif ($arProps["CODE"] == "CITY")
			{
				$city = $arProps['VALUE'];
			}elseif ($arProps["CODE"] == "ADDRESS")
			{
				$adress = $arProps["VALUE"];
			}
			elseif($arProps['CODE'] == 'NAME')
			{
				$arFields['ORDER_USER'] = $arProps['VALUE'];
			}elseif($arProps['CODE'] == 'OTCHESTVO')
			{
				$arFields['ORDER_USER'] = $arProps['VALUE'].' '.$arFields['ORDER_USER'];
			}elseif($arProps['CODE'] == 'FAMILIA')
			{
				$arFields['ORDER_USER'] = $arProps['VALUE'].' '.$arFields['ORDER_USER'];
			}else{
				$strDop .= $arProps['NAME'].": ".$arProps['VALUE']."\r\n\r\n";
			}

		}

		$eventName2 = "SALE_NEW_ORDER_MANAGER";
		$arFieldsManager = 	Array(
				"ORDER_ID" => $arFields["ORDER_ID"],
				"ORDER_DATE" => $arFields["ORDER_DATE"],
				"ORDER_USER" => $arFields['ORDER_USER'],
				"PRICE" => $arFields["PRICE"],
				"EMAIL" => $arFields["SALE_EMAIL"],
				"ORDER_LIST" => $arFields["ORDER_LIST"],
				"SALE_EMAIL" => $arFields["SALE_EMAIL"],
				"DELIVERY_PRICE" => $arFields["DELIVERY_PRICE"],
				'TIME' => date('d.m.o, H:i'),
				'PHONE' => $phone,
				'ADRESS' => $strAdress,
				'DELIVERY' => $delivery['NAME'],
				'COMMENT' => $arOrder['USER_DESCRIPTION'],
				'DOP_INFO' => $strDop,
			);
		$event2 = new CEvent;
		$event2->Send($eventName2, SITE_ID, $arFieldsManager, 35);
	}
}
