<?
AddEventHandler("main", "OnAfterUserAuthorize", Array("SecurityProtected", "OnAfterUserAuthorizeHandler"));

class SecurityProtected
{
	static function ip2number($ip)
	{
		$ip = trim($ip);
		if(strlen($ip) > 0)
			$res = doubleval(sprintf("%u", ip2long(trim($ip))));
		else
			$res = 0;
		return $res;
	}
    // ������� ���������� ������� "OnAfterUserAuthorize"
    function OnAfterUserAuthorizeHandler($arUser)
    {
		global $USER, $_SERVER, $DB;
		if($USER->isAdmin() && CModule::IncludeModule('security')){
			
			$uri = '/bitrix/admin/';
			
			
			$ip2check = self::ip2number($_SERVER["REMOTE_ADDR"]);
			$sql = "SELECT r.ID
			FROM
				b_sec_iprule r
				INNER JOIN b_sec_iprule_incl_mask im on im.IPRULE_ID = r.ID
				LEFT  JOIN b_sec_iprule_excl_mask em on em.IPRULE_ID = r.ID AND '".$DB->ForSQL($uri)."' like em.LIKE_MASK
				INNER JOIN b_sec_iprule_incl_ip   ii on ii.IPRULE_ID = r.ID
				LEFT  JOIN b_sec_iprule_excl_ip   ei on ei.IPRULE_ID = r.ID AND ".$ip2check." between ei.IP_START and ei.IP_END
			WHERE
				r.ACTIVE = 'Y'
				AND (r.ACTIVE_FROM IS NULL OR r.ACTIVE_FROM <= ".$DB->CurrentTimeFunction().")
				AND (r.ACTIVE_TO IS NULL OR r.ACTIVE_TO >= ".$DB->CurrentTimeFunction().")
				
					AND r.ADMIN_SECTION = 'Y'
					
				AND '".$DB->ForSQL($uri)."' like im.LIKE_MASK
				AND em.IPRULE_ID is null
				AND ".$ip2check." between ii.IP_START and ii.IP_END
				AND ei.IPRULE_ID is null";
		
			$rs = $DB->Query($sql);
			if($ar = $rs->Fetch()){
				$USER->Logout();
			}
		}
    }
}