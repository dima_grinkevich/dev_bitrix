<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arParams['BIG_PHOTO_WIDTH'] = intval($arParams['BIG_PHOTO_WIDTH']);
if($arParams['BIG_PHOTO_WIDTH'] <= 0)
	$arParams['BIG_PHOTO_WIDTH'] = 700;
	
$arParams['BIG_PHOTO_HEIGHT'] = intval($arParams['BIG_PHOTO_HEIGHT']);
if($arParams['BIG_PHOTO_HEIGHT'] <= 0)
	$arParams['BIG_PHOTO_HEIGHT'] = 700;
	
$arParams['SMALL_PHOTO_WIDTH'] = intval($arParams['SMALL_PHOTO_WIDTH']);
if($arParams['SMALL_PHOTO_WIDTH'] <= 0)
	$arParams['SMALL_PHOTO_WIDTH'] = 65;
	
$arParams['SMALL_PHOTO_HEIGHT'] = intval($arParams['SMALL_PHOTO_HEIGHT']);
if($arParams['SMALL_PHOTO_HEIGHT'] <= 0)
	$arParams['SMALL_PHOTO_HEIGHT'] = 65;
	


function resizeImage($id, $width, $height, $type = 3){
	$type = $type >= 1 && $type <= 3 ? $type : 3;
	$arTypeResize = array(
		1 => BX_RESIZE_IMAGE_EXACT,
		2 => BX_RESIZE_IMAGE_PROPORTIONAL,
		3 => BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
	);
	return array_change_key_case(CFile::ResizeImageGet(
			 $id, 
			array("width"=>$width, "height"=>$height), 
			$arTypeResize[$type], 
			true, 
			false, 
			false, 
			100
		), CASE_UPPER);
}

function setPreviewPic(&$arElement, $arSettings){
	if(is_array($arElement['PREVIEW_PICTURE'])){
		$mPic = $arElement['PREVIEW_PICTURE'];
	}elseif(is_array($arElement['DETAIL_PICTURE'])){
		$mPic = $arElement['DETAIL_PICTURE'];
	}elseif(intval($arElement['PROPERTIES']['MORE_PHOTO']['VALUE'][0]) > 0){
		$mPic = $arElement['PROPERTIES']['MORE_PHOTO']['VALUE'][0];
	}
	
	if($mPic){
		foreach($arSettings as $k => $setting){
			$width = intval($setting['width']) > 0 ? $setting['width'] : 200;
			$height = intval($setting['height']) > 0 ? $setting['height'] : 200;
			$type = intval($setting['type']);
			$arElement[$k] = resizeImage($mPic, $width, $height, $type);
		}
	}
}

foreach($arResult['ITEMS'] as $key => $arItem){
	$arPhotoID = array();
	if(is_array($arItem['DETAIL_PICTURE'])){
		$arPhotoID[] = array(
			'ID' => $arItem['DETAIL_PICTURE']['ID'],
			'DESCR' => $arItem['DETAIL_PICTURE']['DESCRIPTION']
		);
		
	}
	
	
	if(count($arItem['PROPERTIES']['MORE_FILES_GAL']['VALUE']) > 0 && is_array($arItem['PROPERTIES']['MORE_FILES_GAL']['VALUE'])){
		foreach($arItem['PROPERTIES']['MORE_FILES_GAL']['VALUE'] as $k => $id){
			$arPhotoID[] = array(
			'ID' => $id,
			'DESCR' => $arItem['PROPERTIES']['MORE_FILES_GAL']['DESCRIPTION'][$k]
		);
		}
	}
	
	
	if(count($arPhotoID) > 0){
		$arPhotos = array();
		foreach($arPhotoID as $arPhoto){
			$arPhotos[$arPhoto['ID']] = array_merge(
				$arPhoto,
				array(			
				'BIG' => resizeImage($arPhoto['ID'], $arParams['BIG_PHOTO_WIDTH'], $arParams['BIG_PHOTO_HEIGHT']),
				'SMALL' => resizeImage($arPhoto['ID'], $arParams['SMALL_PHOTO_WIDTH'], $arParams['SMALL_PHOTO_HEIGHT']),
				)
			);
		}
		$arResult['ITEMS'][$key]['PHOTOS'] = $arPhotos;
	}
}


foreach($arResult['ITEMS'] as $key => $arItem){
	if(is_array($arItem['DETAIL_PICTURE'])){	
		$arResult['ITEMS'][$key]['PREVIEW_IMG'] =  array_merge(
			array(
				'ID' => $arItem['DETAIL_PICTURE']['ID'],
				'ALT' => $arItem['DETAIL_PICTURE']['DESCRIPTION'],
			),
			resizeImage($arItem['DETAIL_PICTURE'], $arParams['BIG_PHOTO_WIDTH'], $arParams['BIG_PHOTO_HEIGHT'])
		);
	}
}
?>