<?
CModule::IncludeModule('iblock');
$PROPERTY_ID = 44;
$IBLOCK_ID = 13;

$arTree = array(
    'Conference' => 'Конференция',
    'Exhibition' => 'Выставка',
    'Forum' => 'Форум',
    'Seminar' => 'Семинар',
);

$arEnums = array();
$ibpenum = new CIBlockPropertyEnum;
$rs = CIBlockPropertyEnum::GetList(array('VALUE' => 'ASC'), array('PROPERTY_ID'=>$PROPERTY_ID), false, array('nPageSize' => 10));
while($ar = $rs->Fetch()){
	$arEnums[$ar['ID']] = $ar['VALUE'];
}


$rs = CIblockElement::GetList(array(), array('IBLOCK_ID' => $IBLOCK_ID, '!PROPERTY_NAME_SECTION' => false, 'PROPERTY_TYPE_VALUE' => false), false, array('nPageSize' => 10), array('ID', 'PROPERTY_NAME_SECTION'));
while($ar = $rs->Fetch()){
    $value = array_key_exists($ar['PROPERTY_NAME_SECTION_VALUE'], $arTree) ? $arTree[$ar['PROPERTY_NAME_SECTION_VALUE']] : $ar['PROPERTY_NAME_SECTION_VALUE'];

    $propId = array_search($value, $arEnums);
    if($propId === false){
        $arFields = Array(
    		'PROPERTY_ID' => $PROPERTY_ID,
    		'VALUE'=> $value,
            'XML_ID' => ToLower(array_search($value, $arTree))
    	);

    	if($propId = $ibpenum->Add($arFields)){
            $arEnums[$propId] = $value;
    		echo "<pre>".print_r('New enum '.$value.' ID:'.$propId, true)."</pre>";
    	}else{
    		echo '<pre>Error enum add'.print_r($ibpenum->LAST_ERROR ,true).'</pre>';
    	}

    }
    echo 0;
//echo "<pre>".print_r('Update element: '.$ar['ID'], true)."</pre>";
    CIBlockElement::SetPropertyValuesEx($ar['ID'], $IBLOCK_ID, array($PROPERTY_ID => array('VALUE' => $propId)));

}
